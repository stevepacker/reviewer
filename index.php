<?php
if ((!empty($_GET['debugx']) && '2HwdsiEHfS1k8IIDeOD8' === $_GET['debugx'])
    || __ENV !== 'production')
{
    // specify how many levels of call stack should be shown in each log message
    defined('YII_TRACE_LEVEL') or define('YII_TRACE_LEVEL', 3);
    defined('YII_DEBUG')       or define('YII_DEBUG', true);
} else {
    defined('YII_DEBUG')       or define('YII_DEBUG', false);
}

// use APC version if available
$config = dirname(__FILE__) . '/protected/config/main.php';
$yii    = dirname(__FILE__) . '/protected/yii/';
$yii   .= function_exists('apc_cache_info') && ! YII_DEBUG
        ? 'yiilite.php'
        : 'yii.php';

require_once rtrim(dirname($config), '/') . '/env.php';

require $yii;

// extending CWebApplication so we can run through maintenance mode conditions
require dirname(__FILE__) . '/protected/components/WebApplication.php';

$current_app = Yii::createApplication('WebApplication', $config);
$current_app->run();
