<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentity extends CUserIdentity
{

    private $_id;

    const ERROR_USER_DISABLED       = 101;
    const ERROR_FINANCE_DISABLED    = 102;

    /**
     * Authenticates a user.
     * The example implementation makes sure if the username and password
     * are both 'demo'.
     * In practical applications, this should be changed to authenticate
     * against some persistent user identity storage (e.g. database).
     * @return boolean whether authentication succeeds.
     */
    public function authenticate()
    {
        $user = User::model()->findByAttributes(array('email' => $this->username));

        if ($user === NULL)
        {
            $this->errorCode = self::ERROR_USERNAME_INVALID;
        }
        else if (0 == $user->status)
        {
            $this->errorCode = self::ERROR_USER_DISABLED;
        }
        else if ($user->password !== $user->hashPassword($this->password))
        {
            $this->errorCode = self::ERROR_PASSWORD_INVALID;
        }
        else
        {
            $this->setState('roles', $user->roles);
            $this->errorCode  = self::ERROR_NONE;
            $user->last_ip    = Yii::app()->request->getUserHostAddress();
            $user->last_login = new CDbExpression('NOW()');
            $this->_id = $user->id;
            if (!$user->save())
                throw new CException('Unable to login user: '.print_r($user->errors, true));
        }
        return !$this->errorCode;
    }

    /**
     * This is only to be used when manually authenticating a user for forgotPassword
     */
    public function forceAuthenticate(User $user = null)
    {
        if (null === $user) {
            $user = User::getUser($this->username);
        }
        if ($user) {
            $this->_id = $user->id;
            $this->setState('roles', $user->roles);
            $this->errorCode = self::ERROR_NONE;
        } else {
            $this->errorCode = self::ERROR_USERNAME_INVALID;
        }
        return !$this->errorCode;
    }

    /**
     * Returns the numeric user_id of a user (as required by RBAC)
     *
     * @return int user::id
     */
    public function getId()
    {
        return (int) $this->_id;
    }

}