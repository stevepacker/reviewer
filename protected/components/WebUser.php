<?php
class WebUser extends CWebUser
{
    /**
     * Checks a User's roles for access to an operation.
     *
     * @param string  $operation    Role (in dot-notation)
     * @param object  $subject      Object to be scanned for can* method for the
     *                              operation (example: $operation = "user.edit"
     *                              would call $subject->canEdit($this->user)
     *                              if it exists, and returns false otherwise)
     *                              Note: method MUST take a User object as its
     *                              only argument, but can be an optional argument.
     *
     * @return boolean
     */
    public function checkAccess($operation, $subject = null)
    {
        $roles = (array) $this->getState('roles', array());

        if (in_array(User::ROLE_ADMIN, $roles)) {
            return true;
        }

        // if a user has role "project", user should have "project" and
        // "project.*" access
        foreach ($roles as $role) {
            if ($role === $operation
                ||  0 === strpos($operation, "$role.")) {
                return true;
            }
        }

        if (is_object($subject)) {
            $suffix = trim(strrchr($operation, '.'), '.');
            $method = 'can' . ucfirst(Utils::underscoreToCamel($suffix));
            if (method_exists($subject, $method)) {
                Yii::log(sprintf('Checking access for %s(%s)::%s',
                        get_class($subject),
                        $subject->primaryKey,
                        $method), CLogger::LEVEL_INFO, __CLASS__);
                if ($subject->$method($this->user)) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * Extension on checkAccess, throws CHttpException automatically if
     * access is denied.
     *
     * @param string $role
     * @param array  $params
     * @param string $message
     * @throws CHttpException when access is denied
     */
    public function requireAccess($role, $params = array(), $message = null)
    {
        if (empty($message))
            $message = 'Sorry; you are not allowed to perform this action.  If this
                is in error, please contact support.';

        if (!$this->checkAccess($role, $params))
            throw new CHttpException (403, $message);
    }

    /**
     * Gets the User model from the logged-in user
     * @return User
     */
    public function getUser()
    {
        return User::getUser($this->getId());
    }
}
