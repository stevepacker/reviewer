<?php

class MMongoDB extends EMongoDB
{
    public $replicaSet;

    private $_mongoConnection;

    /**
     * Returns Mongo connection instance if not exists will create new
     *
     * @return Mongo
     * @throws EMongoException
     * @since v1.0
     */
    public function getConnection()
    {
        if($this->_mongoConnection === null)
        {
            try
            {
                Yii::trace('Opening MongoDB connection', 'ext.MongoDb.EMongoDB');
                if(empty($this->connectionString))
                    throw new EMongoException(Yii::t('yii', 'EMongoDB.connectionString cannot be empty.'));

                $params = array('connect' => $this->autoConnect);

                if (!empty($this->replicaSet))
                    $params['replicaSet'] = $this->replicaSet;

                if (!empty($this->persistentConnection))
                    $params['persist']    = $this->persistentConnection;

                $this->_mongoConnection = new Mongo($this->connectionString, $params);

                $this->_mongoConnection->setSlaveOkay(true);

                return $this->_mongoConnection;
            }
            catch(MongoConnectionException $e)
            {
                throw new EMongoException(Yii::t(
                    'yii',
                    'EMongoDB failed to open connection: {error}',
                    array('{error}'=>$e->getMessage())
                ), $e->getCode());
            }
        }
        else
            return $this->_mongoConnection;
    }
}
