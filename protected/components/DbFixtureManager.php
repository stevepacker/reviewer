<?php

Yii::import('system.test.CDbFixtureManager');

class DbFixtureManager extends CDbFixtureManager
{
    public function load($fixtures)
    {
        // autoload models through autoloader, not stupid Yii::import only
        foreach ($fixtures as $fixtureName => $tableName)
            if (':' !== $tableName[0])
                class_exists($tableName, $autoload = true);
            
        return parent::load($fixtures);
    }
}