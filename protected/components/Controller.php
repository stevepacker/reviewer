<?php

/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 */
class Controller extends CController
{
    /**
     * @var string the default layout for the controller view. Defaults to '//layouts/column1',
     * meaning using a single column layout. See 'protected/views/layouts/column1.php'.
     */
    public $layout = '//layouts/column1';
    /**
     * @var array context menu items. This property will be assigned to {@link CMenu::items}.
     */
    public $menu = array();
    /**
     * @var array the breadcrumbs of the current page. The value of this property will
     * be assigned to {@link CBreadcrumbs::links}. Please refer to {@link CBreadcrumbs::links}
     * for more details on how to specify this property.
     */
    public $breadcrumbs = array();

    /**
     * @var stdClass
     * NOTE: this needs to be public for Portlets to work.
     */
    public $view;

    public function init()
    {
        // placeholder for all view-script assignments
        $this->view = new CAttributeCollection();
        $this->view->caseSensitive = true;

        $maintenance_file = Yii::app()->basePath . '/../maintenance.html';
        if (file_exists($maintenance_file) && is_readable($maintenance_file)
//            && !Yii::app()->request->userIsInternal
            )
        {
            header('HTTP/1.1 503 Service Temporarily Unavailable');
            header('Status: 503 Service Temporarily Unavailable');
            header('Retry-After: 60');
            echo file_get_contents($maintenance_file);
            exit;
        }

        parent::init();
    }

    protected function beforeAction($action)
    {
        // create a new user on first login
        if (0 == User::model()->count() && 'create' != $action->id)
            $this->redirect(array('user/create'));

        // make sure jquery is on every page
        Yii::app()->clientScript
            ->registerCoreScript('jquery')
        ;

        // ensure javascript libraries contain basic information about context
        $app = array(
            'baseUrl' => Yii::app()->baseUrl,
        );

        Yii::app()->clientScript
            ->registerScript('_yii_app', '$.app = ' . CJSON::encode($app))
            ->registerScript('_paginate_toTop', '$("ul.yiiPager a").live("click", function(){ $(document).scrollTop(0); });')
            ;

        // do the Yii method
        return parent::beforeAction($action);
    }

    protected function afterAction($action)
    {
        if (Yii::app()->request->isAjaxRequest)
        {
            $this->renderPartial($action->getId(), $this->view->toArray());
//            echo CJSON::encode((array) $this->view);
//            Yii::app()->end();
        }
        else
        {
            $this->render($action->getId(), $this->view->toArray());
        }
        return parent::afterAction($action);
    }

    /**
     * @return array action filters
     */
    public function filters()
    {
        $filters = array();
        if (User::model()->count()) {
            $filters[] = 'accessControl'; // see self::accessRules()
        }

        return $filters;
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            // admins have full access
            array('allow', 'roles'=>array('admin')),
            // authenticated users have access to the "site" controller
            array('allow', 'users'=>array('@')),
            // non-authenticated users have access to only login
            array('allow', 'users'=>array('?'), 'controllers' => array('site'),  'actions' => array(
                'error', 'login', 'contact', 'captcha', 'forgotPassword', 'resetPassword')),
            // and everything else is locked down per-controller
            array('deny'),
        );
    }

    /**
     * Retrieves the User model by PK, and assigns it to the view as well.  Can
     * optionally
     *
     * @param string $id        Primary Key of the model to lookup
     * @param array  $params    List of parameters to check<br>
     *                          * class        ActiveRecord    Model to load.  Default: get_called_class() without "Controller" suffix<br>
     *                          * viewKey      string          Assigns model to the view.  Default: $model<br>
     *                          * assertRole   string          Checks RBAC.  Default: no check<br>
     *                          * assertPost   boolean         Throws 400 error if is not a POST request.  Default: false<br>
     *                          * assertExists boolean         Throws 404 if a model is not found.  Default: true<br>
     *                          * 400message   string          Error message to throw when not a POST request<br>
     *                          * 403message   string          Error message to throw when access is denied<br>
     *                          * 404message   string          Error message to throw when Model is not found<br>
     * @throws CHttpException   On various circumstances.<br>
     *                          * 404   When assertExists is true and a Model is not found<br>
     *                          * 400   Optionally when assertPost is true and fails<br>
     *                          * 403   Optionally when role is defined and fails<br>
     *
     * @return ActiveRecord
     */
    protected function loadModel($id, $params = array())
    {
        if (Arr::get($params, 'assertPost')) {
            if (!Yii::app()->request->isPostRequest) {
                throw new CHttpException(400);
            }
        }

        $defaultClass = preg_replace('/Controller$/', '', get_called_class());
        $class        = Arr::get($params, 'class', $defaultClass);
        $model        = ActiveRecord::model($class)->findByPk($id);
        $viewKey      = Arr::get($params, 'viewKey', lcfirst($defaultClass));
        if (strlen($viewKey)) {
            $this->view->add($viewKey, $model);
        }

        if (Arr::get($params, 'assertExists', true)) {
            if (!$model) {
                throw new CHttpException(404, Arr::get($params, '404message'));
            }
        }


        if ($model instanceof ActiveRecord
            && ($role = Arr::get($params, 'assertRole'))) {
            if (!Yii::app()->user->checkAccess($role, $model)) {
                throw new CHttpException(403, Arr::get($params, '403message'));
            }
        }

        return $model;
    }
}
