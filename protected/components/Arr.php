<?php

/**
 * Array helper
 */
class Arr
{
    /**
     * Avoids warning about indexes not existing and allows default value
     * to be returned in the event a key does not exist in an array.  In the
     * case where an array of keys is sent, only the first key that is found
     * will return its value.
     *
     * @param array  $array
     * @param array|string $keys
     * @param mixed  $default
     * @return mixed Array key's value, or $default if it does not exist
     */
    public static function get($array, $keys, $default = null)
    {
        if (is_scalar($keys))
            $keys = array($keys);
        foreach ($keys as $key) {
            if (isset($array[$key]))
                return $array[$key];
        }
        // if it gets here, no such key exists
        return $default;
    }

    /**
     * EXPERIMENTAL --
     * Like self::get, returns a value of an array for a key if it exists, or
     * a default value if not.  Key can be in dot-notation to permit
     * multi-dimensional array lookup
     *
     * @example self::getDot($array, 'one.two.three') returns $array['one']['two']['three']
     *
     * @param array  $array
     * @param array|string $keys
     * @param mixed  $default
     * @return mixed
     */
    public static function getDot($array, $keys, $default = null)
    {
        if (is_scalar($keys))
            $keys = array($keys);

        foreach ($keys as $dotKey) {
            $values  = $array;
            $break   = false;
            foreach (explode('.', $dotKey) as $key) {
                if (!is_array($values) || !array_key_exists($key, $values)) {
                    // skip to the next in "$keys"
                    $break = true;
                    break;
                }
                $values = $values[$key];
            }
            if (!$break)
                // if it gets here, the key existed and was set to $values
                return $values;
        }
        // no keys fully resolved, so return default
        return $default;
    }

    /**
     * EXPERIMENTAL --
     * Converts a multi-dimensional array into a one-dimenstional
     * array with dot-notation key names
     *
     * @param array $array
     * @return array
     */
    public static function toDot($array, $numeric_arrays = false)
    {
        if (!is_array($array)) {
            if (method_exists($array, 'toArray'))
                $array = $array->toArray();
            else
                return $array;
        }

        return self::_toDot($array, null, $numeric_arrays);
    }

    /**
     * Converts an multi-dimensional array to a CSV format.
     *
     * @requires php5.1
     * @example <pre>data: array(array('header1', 'header2'), array('data1', 'data2'))</pre>
     * @param array $data
     * @param string $delimiter
     * @param string $enclosure
     * @return string
     */
    public static function toCsv($data, $delimiter = null, $enclosure = '"')
    {
        $outstream = fopen("php://temp", 'r+');
        if (is_null($delimiter))
            self::_mssafe_csv($outstream, $data);
        else
            foreach ($data as $row)
                fputcsv ($outstream, $row, $delimiter, $enclosure);
        $outstring = '';
        rewind($outstream);
        while ($line = fgets($outstream))
            $outstring .= $line;
        fclose($outstream);
        return $outstring;
    }

    /**
     *
     * @param string $filepath
     * @param array $data
     * @param <type> $header
     * @return <type>
     */
    private static function _mssafe_csv($filepath, $data, $header = array())
    {
        if (is_resource($filepath))
            $fp = $filepath;
        else if (is_scalar($filepath))
            $fp = fopen($filepath, 'w');

        if ( $fp ) {
            $show_header = true;
            if ( empty($header) ) {
                $show_header = false;
                reset($data);
                $line = current($data);
                if ( !empty($line) ) {
                    reset($line);
                    $first = current($line);
                    if ( substr($first, 0, 2) == 'ID' && !preg_match('/["\\s,]/', $first) ) {
                        array_shift($data);
                        array_shift($line);
                        if ( empty($line) ) {
                            fwrite($fp, "\"{$first}\"\r\n");
                        } else {
                            fwrite($fp, "\"{$first}\",");
                            fputcsv($fp, $line);
                            fseek($fp, -1, SEEK_CUR);
                            fwrite($fp, "\r\n");
                        }
                    }
                }
            } else {
                reset($header);
                $first = current($header);
                if ( substr($first, 0, 2) == 'ID' && !preg_match('/["\\s,]/', $first) ) {
                    array_shift($header);
                    if ( empty($header) ) {
                        $show_header = false;
                        fwrite($fp, "\"{$first}\"\r\n");
                    } else {
                        fwrite($fp, "\"{$first}\",");
                    }
                }
            }
            if ( $show_header ) {
                fputcsv($fp, $header);
                fseek($fp, -1, SEEK_CUR);
                fwrite($fp, "\r\n");
            }
            foreach ( $data as $line ) {
                fputcsv($fp, $line);
                fseek($fp, -1, SEEK_CUR);
                fwrite($fp, "\r\n");
            }
            if ($fp !== $filepath)
                fclose($fp);
        } else {
            return false;
        }
        return true;
    }

    /**
     * Resursively walks through an array and converts keys to dot-notation
     *
     * @param array $array
     * @param string|null $keyname
     * @param boolean $numeric_arrays whether to return a key's value as
     *                                an array when only numeric keys exist
     * @return array
     */
    private static function _toDot($array, $keyname = null, $numeric_arrays = false)
    {
        $output = array();
        foreach ($array as $key => $value) {
            $key = is_null($keyname)
                 ? $key
                 : "$keyname.$key";
            if (!is_array($value)) {
                $output[$key] = $value;
            } elseif (!$numeric_arrays) {
                $output += self::_toDot($value, $key, $numeric_arrays);
            } else {
                $keys    = array_keys($value);
                $numeric = true;
                foreach (array_reverse($keys) as $k)
                    if (!is_numeric($k) && is_scalar($value[$k]))
                        $numeric = false;
                if ($numeric && 0 === $k)
                    $output[$key] = $value;
                else
                    $output += self::_toDot($value, $key, $numeric_arrays);
            }
        }
        return $output;
    }

    /**
     * Converts a string into a multi-dimensional array
     *
     * @param string $string
     * @return array
     */
    public static function dot2array($string, $value = null)
    {
        $string = trim($string);
        if (false === strpos($string, '.'))
            return array($string => $value);

        $key   = strstr($string, '.', true);
        $value = self::dot2array(substr($string, strlen($key)+1), $value);

        return array($key => $value);
    }

    /**
     * Returns an array with the values for the keys provided.  Note if a key
     * doesn't exist in the $array, the $default value is assigned to it instead.
     *
     * @param array $array
     * @param array $keys
     * @param mixed $default
     * @return array
     */
    public static function getAll($array, $keys, $default = null)
    {
        $return = array();
        foreach ($keys as $key)
            $return[$key] = self::get($array, $key, $default);
        return $return;
    }

    /**
     * Returns an array of values from each element in the given array
     * with the given key.
     *
     * @param array  $array
     * @param string $key
     * @param mixed  $default
     */
    public static function getMultiDim($array, $key, $default = null)
    {
        $return = array();
        foreach ($array as $element_key => $element) {
            $return[$element_key] = self::get($element, $key, $default);
        }

        return $return;
    }

    /**
     * Merge two arrays together where $two's values overwrite $one's values.
     *
     * @param array $one
     * @param array $two
     * @return array
     */
    public static function merge($one, $two)
    {
        foreach ($two as $key => $value) {
            if (array_key_exists($key, $one) && is_array($value))
                $one[$key] = self::merge($one[$key], $two[$key]);
            else
                $one[$key] = $value;
        }
        return $one;
    }

    /**
     * Sorts a multi-dimensional array on a value ascending or descending
     *
     * @param array $array to search
     * @param string $value to sort on
     * @param string $direction to sort in (SORT_ASC / SORT_DESC)
     * @return byref
     */
    public static function multidimSortOnValue(&$array, $value, $direction = SORT_ASC)
    {
        $keys = array();

        foreach ($array as $key => $row)
            $keys[$key] = Arr::get($row, $value);

        array_multisort($keys, $direction, $array);
    }

    /**
     * Diffs two multi-dimensional arrays and returns the difference as a flattened array
     *
     * @param array $array1
     * @param array $array2
     * @returns array $diff
     */
    public static function multidimDiff($array1, $array2)
    {
        if (!is_array($array1) || !is_array($array2))
            return false;

        $flatArr1 = self::_multidimFlatten($array1);
        $flatArr2 = self::_multidimFlatten($array2);

        return array_diff_assoc($flatArr2, $flatArr1);
    }

    /**
     * Flattens a multi-dimensional array so it can be diffed with another array using array_diff
     *
     * @param array $array1
     * @returns array $result
     */
    private static function _multidimFlatten($array1, $base = "", $divider_char = ".")
    {
        $ret = array();

        if (is_array($array1))
        {
            foreach($array1 as $key => $value)
            {
                if (is_array($value))
                {
                    $tmp_array = self::_multidimFlatten($value, $base . $key . $divider_char, $divider_char);
                    $ret = array_merge($ret, $tmp_array);
                }
                else
                    $ret[$base . $key] = $value;
            }
        }

        return $ret;
    }


    /**
     * Generates a Excel-2007 (XLSX) spreadsheet given an array of data and headers.
     *
     * @param array $data
     * @param array $headers
     * @param mixed $output <pre>
     *        string: Filename to write spreadsheet to
     *        false:  PHPExcel object
     *        true:   Pushes file (with headers) to browser</pre>
     * @return PHPExcel
     */
    public static function toExcel($data, $headers = array(), $output = false)
    {
        // disable autoloader so PHPExcel can use its own
        $phpExcelPath = Yii::getPathOfAlias('ext.phpexcel');
        spl_autoload_unregister(array('YiiBase','autoload'));
        include_once($phpExcelPath . DIRECTORY_SEPARATOR . 'PHPExcel.php');

        // catch exceptions and re-instantiate Yii autoloader before throwing
        try
        {
            $xls = new PHPExcel();
            $xls->setActiveSheetIndex(0);
            PHPExcel_Shared_Font::setAutoSizeMethod(PHPExcel_Shared_Font::AUTOSIZE_METHOD_EXACT);


            self::$_rowCells = array(); // reset static array before generating sheet
            foreach ($headers as $y => $header)
                foreach ($header as $x => $value)
                    self::_insertAndFormatExcelCell($xls, $x, $y, $value);

            $header_count = count($headers);
            foreach ($data as $y => $row)
                foreach ($row as $x => $value)
                    self::_insertAndFormatExcelCell($xls, $x, $y+$header_count, $value);

            // freeze headers
            if (!empty($header_count))
                try {
                    $freeze_cell = 'A' . ($header_count + 1);
                    @$xls->getActiveSheet()->freezePane($freeze_cell);
                } catch (Exception $e) { /* silence this fluf feature*/ }

            if ($output)
            {
                if (is_scalar($output))
                {
                    // Excel2007 (xlsx), Excel2005 (xls), PDF, HTML
                    $objWriter = PHPExcel_IOFactory::createWriter($xls, 'Excel2007');
                    $objWriter->setOffice2003Compatibility(true);
                    $objWriter->save($output);
                }
                else
                {
                    // Redirect output to a client’s web browser (Excel2007)
                    header('Content-Type: application/pdf');
                    header('Content-Disposition: attachment;filename="01simple.xlsx"');
                    header('Cache-Control: max-age=0');

                    $objWriter = PHPExcel_IOFactory::createWriter($xls, 'Excel2007');
                    $objWriter->save('php://output');
                    Yii::app()->end();
                }
            }
        }
        catch (Exception $e)
        {
            spl_autoload_register(array('YiiBase','autoload'));
            throw $e;
        }

        // give autoloader privledge back to Yii
        spl_autoload_register(array('YiiBase','autoload'));

        return $xls;
    }

    /**
     * @var array count of spanned columns per row
     */
    private static $_rowCells = array();

    /**
     *
     * @param PHPExcel $xls
     * @param int $x
     * @param int $y
     * @param string|array $value
     * @example of $value: <pre>
     * array(
     *   'value' => 'Cell Contents',
     *   'span'  => 2,
     *   'color' => '#cccccc', // important to define all 6; CSS shortcut of doing 3 letters does not work
     * )
     * </pre>
     */
    private static function _insertAndFormatExcelCell(PHPExcel $xls, $x, $y, $value)
    {
        $y++; // excel rows start at 1, not zero
        $merges  = Arr::get(self::$_rowCells, $y, 0);
        $coord_x = $merges + $x;
        $coord   = self::_convertRowNumberToLetters($coord_x) . $y;
        $cell    = $xls->getActiveSheet()->getCell($coord);
        $type    = PHPExcel_Cell_DataType::TYPE_STRING;
        $content = is_scalar($value)
                 ? $value
                 : Arr::get($value, 'value', '');

        if (is_numeric($content) || ('$' === substr($content, 0, 1) && is_numeric(ltrim($content, '$'))))
        {
            $type    = PHPExcel_Cell_DataType::TYPE_NUMERIC;
            if ('$' === $content[0])
            {
                $content = ltrim($content, '$');
                $xls->getActiveSheet()->getStyle($coord)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_CURRENCY_USD_SIMPLE);
            }
        }

        $cell->setValueExplicit($content, $type);
//        echo "$coord: x: $x, y: $y -- merges: $merges -- content: $content";

        if (is_array($value))
        {
            $span = $coord;
            if (($span  = Arr::get($value, 'span', $coord)) && is_numeric($span))
            {
//                echo " -- span: $span";
                @self::$_rowCells[$y] += $span-1;
                $span = $coord . ':' . self::_convertRowNumberToLetters($coord_x+$span-1) . $y;
//                echo " -- coord: $span";
            }


            $styleAttribs = array();
            if (($color = Arr::get($value, 'background')))
                $styleAttribs['fill'] = array(
                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                    'startcolor' => array('argb' => 'FF' . strtoupper(ltrim($color, '#'))),
                );
            if (($border = Arr::get($value, 'border', 1)))
                $styleAttribs['borders'] = array(
                    'allborders' => array(
                        'style' => PHPExcel_Style_Border::BORDER_THIN,
                    ),
                );

            if (!empty($styleAttribs))
                $xls->getActiveSheet()->getStyle($span)->applyFromArray($styleAttribs);

            if ($span != $coord)
                $xls->getActiveSheet()->mergeCells($span);
        }
//        echo PHP_EOL;
    }

    /**
     * Converts an integer to a letter for Excel columns
     *
     * @param int $int zero-based, so 0=A, 1=B, etc.
     * @return string $value
     */
    private static function _convertRowNumberToLetters($int)
    {
        $value = 'A';
        while (--$int >= 0)
            $value++;
        return $value;
    }
}
