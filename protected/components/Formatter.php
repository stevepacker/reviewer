<?php

/**
 * CFormatter overrides
 */
class Formatter extends CFormatter
{
	/**
	 * Formats the value as a date.
	 * @param mixed $value the value to be formatted
	 * @return string the formatted result
	 * @see dateFormat
	 */
	public function formatDate($value)
	{
        if (is_string($value))
            $value = strtotime($value);
        return parent::formatDate($value);
	}

	/**
	 * Formats the value as a time.
	 * @param mixed $value the value to be formatted
	 * @return string the formatted result
	 * @see timeFormat
	 */
	public function formatTime($value)
	{
        if (is_string($value))
            $value = strtotime($value);
		return parent::formatTime($value);
	}

	/**
	 * Formats the value as a date and time.
	 * @param mixed $value the value to be formatted
	 * @return string the formatted result
	 * @see datetimeFormat
	 */
	public function formatDatetime($value)
	{
        if (is_string($value))
            $value = strtotime($value);
		return parent::formatDatetime($value);
	}

    /**
     * Returns a de-Microsoft'd string
     *
     * @link http://www.php.net/manual/en/function.chr.php#85050
     * @param string $str
     * @return string
     */
    public static function deMicrosoftText($str)
    {
        $replacements = array(
            "'" => array(
                chr(145),
                chr(146),
                '‘', '’', '‛'
            ),
            '"' => array(
                chr(132),
                chr(147),
                chr(148),
                '“', '”',
            ),
            ','   => array(
                chr(130), // baseline single quote
                '‚',
            ),
            'NLG' => chr(131), // florin
            '...' => array(
                chr(133), // ellipsis
                '…',
            ),
            '**'  => array(
                chr(134), // dagger (a second footnote)
                '†',
            ),
            '***' => array(
                chr(135), // double dagger (a third footnote)
                '‡',
            ),
            '^'   => chr(136), // circumflex accent
            'o/oo'=> array(
                chr(137), // permile
                '‰',
            ),
            'Sh'  => chr(138), // S Hacek
            '<'   => array(
                chr(139), // left single guillemet
                '‹',
            ),
            '>'   => array(
                chr(155), // right single guillemet
                '›',
            ),
            'OE'  => chr(140), // OE ligature
            '-'   => array(
                chr(149), // bullet
                '•',
                chr(150), // endash
            ),
            '--'  => array(
                chr(151), // emdash
                '–',
            ),
            '~'   => chr(152), // tilde accent
            '(TM)'=> array(
                chr(153), // trademark ligature
                '™',
            ),
            'sh'  => chr(154), // s Hacek
            'oe'  => chr(156), // oe ligature
            'Y'   => chr(159), // Y Dieresis
        );
        foreach ($replacements as $value => $keys)
        {
            $str = str_replace($keys, $value, $str);
        }
        return $str;
    }

    /**
     * Function to filter predefined unallowed HTML tags from a string
     * @param mixed $value
     * @return string String strip of unallowed tags
     */
    public static function stripUnallowedTags($value)
    {
        $allowedTags = '<em><strong><br><hr><sub><sup><b><i>';
        // Deprecated tags not allowed in HTML 5 but allowing for backward compatibility
        $allowedTags .= '<u>';
        return strip_tags($value, $allowedTags);
    }
}
