<?php
/**
 * Ip2LongBehavior class file.
 *
 * @author Stephen Packer <stephen.packer@business.com>
 */

 /**
  * Ip2LongBehavior will automatically convert IPv4 addresses to INT values
  * for insert/updates, and vice-versa for finds.
  *
  * You may specify an active record model to use this behavior like so:
  * <pre>
  * public function behaviors(){
  * 	return array(
  * 		'Ip2LongBehavior' => array(
  * 			'class'     => 'Ip2LongBehavior',
  * 			'attribute' => 'ip_address',
  * 		)
  * 	);
  * }
  * </pre>
  */

class Ip2LongBehavior extends CActiveRecordBehavior {
	/**
	* @var string The name of the attribute to store the creation time.
	*/
	public $attribute;

	/**
	* Responds to {@link CModel::onBeforeSave} event.
	* Sets the values of the IP Address attribute as configured
	*
	* @param CModelEvent $event event parameter
	*/
	public function beforeSave($event) {
		$this->getOwner()->{$this->attribute} = ip2long($this->getOwner()->{$this->attribute});
	}

    /**
	* Responds to {@link CModel::onAfterFind} event.
	* Sets the values of the IP Address attribute as configured
	*
	* @param CModelEvent $event event parameter
	*/
	public function afterFind($event) {
		$this->getOwner()->{$this->attribute} = long2ip($this->getOwner()->{$this->attribute});
	}

}