<?php

class Git_Repository extends AGitRepository
{
    public $enableCache = true;

    /**
     * Duration for cache to persist
     */
    const CACHE_SECONDS = 3600;

    /**
     * Generates the cache dependency object for git command caching
     *
     * @return CDbCacheDependency
     */
    private function _cacheDependency()
    {
        $directory = basename($this->getPath());
        $project   = Project::model()
            ->cache(self::CACHE_SECONDS)
            ->directoryScope($directory)
            ->find();

        if (null === $project) {
            return;
        }
        
        $dependency = new CDbCacheDependency('
            SELECT updated
            FROM `projects`
            WHERE `name` = :name
            LIMIT 1');
        $dependency->params[':name'] = $project->name;

        return $dependency;
    }

    /**
     * Override of parent::run() to allow caching
     *
     * @param string $command
     * @param boolean $saveToCache
     * @return string
     */
    public function run($command, $saveToCache = true) {
        $cacheKey = sprintf('gitrepo-%s-%s', $this->getPath(), $command);

        $results  = false;
        if ($this->enableCache)
           $results = Yii::app()->cache->get($cacheKey);

        if (false === $results) {
            $results = parent::run($command);
            if ($saveToCache) {
                Yii::log('Caching results from command: ' . $command, CLogger::LEVEL_INFO, __CLASS__);
                Yii::app()->cache->set(
                    $cacheKey,
                    $results,
                    self::CACHE_SECONDS,
                    $this->_cacheDependency());
            }
        }
        return $results;
    }

	/**
	 * Gets a commit by its hash
	 * @param string $hash 40 chararcter commit hash of the commit
	 * @return AGitCommit|null
	 */
	public function getCommit($hash)
	{
		if (strlen($hash) < 40) {
			throw new AGitException('Abbreviated commit hashes are not supported yet.');
		}

		if (!isset($this->_commits[$hash])) {
			if($this->hasCommit($hash)){
				$commit = new Git_Commit($hash, $this);
				$this->_commits[$hash] = $commit;
			}else{
				return null;
			}
		}

		return $this->_commits[$hash];
	}

    /**
     * Pulls latest code from remote (origin)
     *
     * @return string
     */
    public function pull()
    {
        return $this->run('remote update', false);
    }

    /**
     * Locally cloned repos don't need the filesystem; just mirror
     *
     * @param string $sourceUrl
     * @return string
     */
    public function cloneRemote($sourceUrl)
    {
        $sourceUrl = ' --mirror ' . $sourceUrl;

        return parent::cloneRemote($sourceUrl);
    }
}