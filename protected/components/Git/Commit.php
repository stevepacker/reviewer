<?php

class Git_Commit extends AGitCommit
{
    public function getAuthor()
    {
        return sprintf('%s <%s>', $this->authorName, $this->authorEmail);
    }

    public function getBranches()
    {
        $rawBranches = $this->repository->run('branch --contains ' . $this->hash);
        $branches = explode("\n", $rawBranches);
        foreach ($branches as $i => $branch) {
            $branches[$i] = trim($branch, " *\t");
        }

        return $branches;
    }
}
