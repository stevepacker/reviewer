<?php
Yii::import('zii.widgets.CPortlet');

class Portlet extends CPortlet
{
    /**
     * Designates the file (without extension) of the view-script
     *
     * @var string
     */
    public $view;

    /**
     * Any additional parameters that should be passed to the view-script can
     * be done via this variable.  The controller variables will similarly be
     * passed to the view-script for rendering.
     *
     * @var array
     */
    public $params = array();

    /**
     * Ran when widget originally gets instantiated
     */
    public function init()
    {
        ob_start();
		ob_implicit_flush(false);
    }

    /**
     * Ran when widget is intended to be rendered
     *
     * @return null
     */
    public function run()
    {
        $content = $this->renderContent();
        if (!is_string($content))
            $content = ob_get_contents();

        ob_end_clean();

        if ($this->hideOnEmpty && '' === trim($content))
			return;

        echo $content;
    }

    /**
     * Override CWidget method to point to the proper directory
     *
     * @param boolean $checkTheme
     * @return string Path to the view-script directory
     */
    public function  getViewPath($checkTheme = false)
    {
        $className  = get_class($this);
        $widget_dir = join(DIRECTORY_SEPARATOR, array(
            $this->getOwner()->getViewPath(),
            '..',
            'widgets',
        ));
        return realpath($widget_dir);
    }

    /**
     * Clips are stored on the CController level, so get from Owner.
     *
     * @return CMap (array imitation)
     */
    public function getClips()
    {
        return $this->getOwner()->getClips();
    }

    /**
     * Generates HTML code, which self::run() will wrap.
     *
     * @return string $content
     */
    protected function renderContent()
    {
        // descend view variables from Controller through to Portlet
        $owner  = $this->owner;
        $params = $this->params;
        while (false === ($owner instanceof CController))
        {
            $params = Arr::merge((array) $owner->params, $params);
            $owner  = $owner->owner;
        }
        $params = Arr::merge((array) $owner->view, $this->params);

        if (method_exists($this, 'beforeRenderContent'))
            call_user_func(array($this, 'beforeRenderContent'));

        if (!$this->getViewFile($this->view))
        {
            Yii::log('Unable to render Portlet: ' . $this->view, 'warning', get_called_class());
            return;
        }

        // echo's and renders get caught by a output buffer in self::init()
        $this->render($this->view, $params);
    }


    /**
	 * Creates a relative URL for the specified action defined in this controller.
	 * @param string $route the URL route. This should be in the format of 'ControllerID/ActionID'.
	 * If the ControllerID is not present, the current controller ID will be prefixed to the route.
	 * If the route is empty, it is assumed to be the current action.
	 * Since version 1.0.3, if the controller belongs to a module, the {@link CWebModule::getId module ID}
	 * will be prefixed to the route. (If you do not want the module ID prefix, the route should start with a slash '/'.)
	 * @param array $params additional GET parameters (name=>value). Both the name and value will be URL-encoded.
	 * If the name is '#', the corresponding value will be treated as an anchor
	 * and will be appended at the end of the URL. This anchor feature has been available since version 1.0.1.
	 * @param string $ampersand the token separating name-value pairs in the URL.
	 * @return string the constructed URL
	 */
    public function createUrl($route, $params=array(), $ampersand='&')
    {
        return $this->getOwner()->createUrl($route, $params, $ampersand);
    }

    /**
     * String used to set session value for magic variable purposes
     * @return string
     */
    public function getSessionKey()
    {
        return get_called_class() . '_' . $this->view;
    }
}
