<?php

//just general utility functions, for when you're too lazy to type out stuff...

class Utils
{
    /**
     * Send a POST requst using cURL
     *
     * @link http://www.php.net/manual/en/function.curl-exec.php#98628
     * @param string $url to request
     * @param array $post values to send
     * @param array $options for cURL
     * @return string
     */
    public static function curl_post($url, $post = array(), $options = array())
    {
        $defaults = array(
            CURLOPT_POST            => 1,
            CURLOPT_HEADER          => 0,
            CURLOPT_URL             => $url,
            CURLOPT_FRESH_CONNECT   => 1,
            CURLOPT_RETURNTRANSFER  => 1,
            CURLOPT_FORBID_REUSE    => 1,
            CURLOPT_TIMEOUT         => 4,
            CURLOPT_POSTFIELDS      => http_build_query($post)
        );

        $ch = curl_init();
        curl_setopt_array($ch, ($options + $defaults));
        if( ! $result = curl_exec($ch))
        {
            throw new CException(curl_error($ch));
        }
        curl_close($ch);
        return $result;
    }

    /**
     * Send a GET requst using cURL
     *
     * @link http://www.php.net/manual/en/function.curl-exec.php#98628
     * @param string $url to request
     * @param array $get values to send
     * @param array $options for cURL
     * @return string
     */
    public static function curl_get($url, $get = array(), $options = array())
    {
        $defaults = array(
            CURLOPT_URL             => $url . (strpos($url, '?') === FALSE ? '?' : '') . http_build_query($get),
            CURLOPT_HEADER          => 0,
            CURLOPT_RETURNTRANSFER  => TRUE,
            CURLOPT_TIMEOUT         => 4
        );

        $ch = curl_init();
        curl_setopt_array($ch, ($options + $defaults));
        if( ! $result = curl_exec($ch))
        {
            throw new CException(curl_error($ch));
        }
        curl_close($ch);
        return $result;
    }

    /**
     * Returns an API key for validating transmission of cache rebuilds on CCM => Public
     *
     * @param int $time
     * @return string
     */
    public static function generateCacheApiKey($time = null)
    {
        if (is_null($time))
            $time = time();
        return substr(md5('api' . $time . Arr::get(Yii::app()->params, 'api_key')), 5, 6);
    }

   /**
    * Translates a camel case string into a string with underscores
    *
    * @example Utils::camelToUnderscore('firstName'); // returns "first_name"
    * @param   string $str String in camel case format
    * @return  string $str Translated into underscore format
    */
    public static function camelToUnderscore($str)
    {
        $output = '';
        $bits   = preg_split('/([A-Z])/', $str, null, PREG_SPLIT_DELIM_CAPTURE);
        for ($i=0,$c=count($bits); $i<$c; $i++)
            $output .= (1 === $i%2)
                     ? strtolower("_{$bits[$i]}")
                     : strtolower($bits[$i]);
        return trim($output, '_');
    }

   /**
    * Translates a string with underscores into camel case
    *
    * @example Utils::underscoreToCamel('first_name'); // returns "firstName"
    * @param   string $str String in underscore format
    * @param   boolean $capitalise_first_char
    * @return  string $str Translated into camel case format
    */
    public static function underscoreToCamel($str, $capitalise_first_char = false)
    {
        $bits   = explode('_', $str);
        $output = $capitalise_first_char
                ? ucfirst($bits[0])
                : $bits[0];
        for ($i=1,$c=count($bits); $i<$c; $i++)
            $output .= ucfirst($bits[$i]);
        return $output;
    }

   /**
    * Get the diff between 2 fields
    *
    * @param   string $str String of origin field
    * @param   string $cmp String of comparison field
    * @return  string diff between 2 fields
    */
    public static function diffString($str, $cmp)
    {
        if(strcmp($str, $cmp) == "0")
            return;

        return self::htmlDiff($str, $cmp);
    }



    public static function diff($old, $new){
        $maxlen = 0;
        foreach($old as $oindex => $ovalue){
            $nkeys = array_keys($new, $ovalue);
            foreach($nkeys as $nindex){
                $matrix[$oindex][$nindex] = isset($matrix[$oindex - 1][$nindex - 1]) ?
                $matrix[$oindex - 1][$nindex - 1] + 1 : 1;
                if($matrix[$oindex][$nindex] > $maxlen){
                    $maxlen = $matrix[$oindex][$nindex];
                    $omax = $oindex + 1 - $maxlen;
                    $nmax = $nindex + 1 - $maxlen;
                }
            }
        }
        if($maxlen == 0) return array(array('d'=>$old, 'i'=>$new));
        return array_merge(
            self::diff(array_slice($old, 0, $omax), array_slice($new, 0, $nmax)),
            array_slice($new, $nmax, $maxlen),
            self::diff(array_slice($old, $omax + $maxlen), array_slice($new, $nmax + $maxlen)));
    }

    public static function htmlDiff($old, $new)
    {
        $ret = '';
        $diff = self::diff(explode(' ', $old), explode(' ', $new));
        foreach ($diff as $k) {
            if (is_array($k)) {
                $ret .= (!empty($k['d'])?"<i>".implode(' ',$k['d'])."</i> ":'').
                (!empty($k['i'])?"<del>".implode(' ',$k['i'])."</del> ":'');
            } else {
                $ret .= $k . ' ';
            }
        }
        return $ret;
    }
}