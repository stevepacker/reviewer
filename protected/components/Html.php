<?php

class Html extends CHtml
{

    /**
     * Returns the <img> tag of a gravitar
     *
     * @param string $email
     * @param array $params Various parameters, including:
     *      default:    Default
     * @return type
     */
    public static function gravitar($email, $params = array())
    {
        $grav_url  = 'https://secure.gravatar.com/avatar/';
        $grav_url .= md5(strtolower(trim($email))) . '?';

        foreach (array(
            'd' => 'default',
            'r' => 'rating',
            's' => 'size') as $letter => $key) {
            if (($value = Arr::get($params, $key))) {
                $grav_url .= sprintf('&%s=%s', urlencode($letter), urlencode($value));
            }
            else if (($value = Arr::get($params, $letter))) {
                $grav_url .= sprintf('&%s=%s', urlencode($letter), urlencode($value));
            }
        }

        $htmlOptions = Arr::get($params, 'htmlOptions', array());
        $htmlOptions['src'] = $grav_url;

        return self::tag('img', $htmlOptions);
    }

    public static function dl_errors($errors = array())
    {
        foreach ($errors as $field => $messages) {
            if (is_array($messages) && !empty($messages) && !empty($field)) {
                $line  = self::tag('dt', array(), self::encode($field));
                foreach ($messages as $message) {
                    $line .= self::tag('dd', array(), self::encode($message));
                }
            }
            $line  = self::tag('dl', array(), $line);
        }
    }
}