<?php

class WebApplication extends CWebApplication
{
    public function init()
    {
        parent::init();
        
        $maintenance_file = Yii::app()->basePath . '/../maintenance.html';
        if (file_exists($maintenance_file) && is_readable($maintenance_file)
            && !Yii::app()->request->userIsInternal
            )
        {
            header('HTTP/1.1 503 Service Temporarily Unavailable'); 
            header('Status: 503 Service Temporarily Unavailable'); 
            header('Retry-After: 60');
            echo file_get_contents($maintenance_file);
            exit;
        }
        
    }
}