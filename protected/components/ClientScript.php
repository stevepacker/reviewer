<?php
class ClientScript extends CClientScript
{
    private static $_version;

    public $custom = array();

	/**
	 * Registers a CSS file
	 * @param string $url URL of the CSS file
	 * @param string $media media that the CSS file should be applied to. If empty, it means all media types.
	 * @return CClientScript the CClientScript object itself (to support method chaining, available since version 1.1.5).
	 */
//	public function registerCssFile($url, $media='')
//	{
//        $parsed  = parse_url($url);
//        if (empty($parsed['host']) || false !== strpos($parsed['host'], 'business.com'))
//        {
//            parse_str(Arr::get($parsed, 'query'), $query);
//            $query['v']      = self::_getVersion();
//            $parsed['query'] = http_build_query($query);
//            $url             = Utils::glue_url($parsed);
//        }
//        return parent::registerCssFile($url, $media);
//	}

    /**
     * Registers a custom block of HTML code for the head
     *
     * @param string $key
     * @param string $content
     * @return ClientScript
     */
    public function registerCustomHeader($key, $content)
    {
        $this->custom[$key] = $content;
        return $this;
    }

    private static function _getVersion()
    {
        self::$_version = (int) Yii::app()->db->createCommand()
            ->select('value')
            ->from('settings')
            ->where('`key` = "clientScript.version"')
            ->queryScalar();
        return self::$_version;
    }

	/**
	 * Inserts the scripts in the head section.
	 * @param string $output the output to be inserted with scripts.
	 */
	public function renderHead(&$output)
	{
		$html='';
		foreach($this->metaTags as $meta)
			$html.=CHtml::metaTag($meta['content'],null,null,$meta)."\n";
		foreach($this->linkTags as $link)
			$html.=CHtml::linkTag(null,null,null,null,$link)."\n";
		foreach($this->cssFiles as $url=>$media)
			$html.=CHtml::cssFile($url,$media)."\n";
		foreach($this->css as $css)
			$html.=CHtml::css($css[0],$css[1])."\n";
		if($this->enableJavaScript)
		{
			if(isset($this->scriptFiles[self::POS_HEAD]))
			{
				foreach($this->scriptFiles[self::POS_HEAD] as $scriptFile)
					$html.=CHtml::scriptFile($scriptFile)."\n";
			}

			if(isset($this->scripts[self::POS_HEAD]))
				$html.=CHtml::script(implode("\n",$this->scripts[self::POS_HEAD]))."\n";
		}
        $html .= join(PHP_EOL, $this->custom) . PHP_EOL;

		if($html!=='')
		{
			$count=0;
			$output=preg_replace('/(<\\/head\s*>)/is','<###head###>$1',$output,1,$count);
			if($count)
				$output=str_replace('<###head###>',$html,$output);
			else
				$output=$html.$output;
		}
	}
}