<?php

/**
 * Autoloader that emulates the Zend_Loader_Autoloader class by using underscores
 * as directory separators.
 *
 * @example A class "FeaturedListing_Copy" would be sought in the directory
 * "components/FeaturedListing/Copy.php" and "models/FeaturedListing/Copy.php"
 * and other directories until it is found, included, and the class_exists().
 */
class AutoLoader extends CApplicationComponent
{
    /**
     * Singleton object holder
     *
     * @var AutoLoader
     */
    private static $_instance;

    /**
     * Set to true, this will execute this autoloader before the built-in Yii
     * autoloader.  Otherwise, it is appended and ran after Yii's autoloader runs.
     *
     * @var boolean
     */
    public $append_autoloader = false;

    /**
     * Delimiter for converting classNames into directories.
     *
     * @var string
     */
    public $delimiter           = '_';

    /**
     * Toggles requirement of classNames including the parentPath.
     * @example if set to "true", a file with the filename "models/Foo/Bar.php"
     * would require a className "Model_Foo_Bar".
     * @example if set to "false", a file with the filename "models/Foo/Bar.php"
     * may have a className "Foo_Bar", and the autoloader would look in the
     * directories "models/Foo", "components/Foo", and others for the file "Bar.php"
     *
     * @var boolean $require_parent_dir
     */
    public $require_parent_dir  = false;

    /**
     * A list of directories at Yii::app()->basePath to search for a particular
     * className.  Note that by entering directories, the autoloader will
     * ignore $this->exclude_parent_dirs.
     *
     * @var array
     */
    public $include_parent_dirs = array();

    /**
     * A list of directories at Yii::app()->basePath to not be auto-searched
     *
     * @var array
     */
    public $exclude_parent_dirs = array();

    /**
     * Cache of the glob result for load()
     *
     * @var array
     */
    private $_glob;

    /**
     * Register autoloader with Yii.
     *
     * @return null
     */
    public function init()
    {
        self::$_instance = $this;
        Yii::registerAutoloader(
            array(get_class($this), 'load'),
            $this->append_autoloader
        );
        return parent::init();
    }

    /**
     * Singleton instantiator
     *
     * @return AutoLoader
     */
    public static function getInstance()
    {
        return self::$_instance
             ? self::$_instance
             : self::$_instance = new self;
    }

    /**
     * Static method to instantiate singleton and load a class
     *
     * @param string $className
     */
    public static function load($className)
    {
        self::getInstance()->autoload($className);
    }

    /**
     * Does actual autoloading work.  Attempts to resolve a className to a file
     * path and load it.
     *
     * @param string $className
     * @return null
     */
    public function autoload($className)
    {
        $dirs  = explode($this->delimiter, $className);
        $paths = array();
        if ($this->require_parent_dir)
        {
            // convert first dirname into filesystem directory
            $dirs[0] = strtolower($dirs[0] . 's');
            $paths[] = $dirs;
        }
        else
        {
            if (!empty($this->include_parent_dirs)) {
                foreach ($this->include_parent_dirs as $dir) {
                    $paths[] = array_merge(array($dir), $dirs);
                }
            } else {
                // search in all class dirs
                if (empty($this->_glob))
                    $this->_glob = glob(sprintf('%s/*s', Yii::app()->basePath), GLOB_ONLYDIR | GLOB_NOSORT);
                foreach ($this->_glob as $path)
                {
                    if (!in_array($path, $this->exclude_parent_dirs))
                    {
                        $path    = basename($path);
                        $paths[] = array_merge(array($path), $dirs);
                    }
                }
            }
        }

        // Generate filepath && include the file containing the className
        foreach ($paths as $path)
        {
            array_unshift($path, Yii::app()->basePath);
            $filepath = implode(DIRECTORY_SEPARATOR, $path) . '.php';
            if (file_exists($filepath))
            {
                include_once($filepath);
                break;
            }
        }
        return class_exists($className, false) || interface_exists($className, false);
    }
}