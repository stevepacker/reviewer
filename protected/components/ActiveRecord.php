<?php

/**
 * Convience class that wraps CActiveRecord with methods
 *
 * @requires PHPv5.3
 */
class ActiveRecord extends CActiveRecord
{
    const INT_SIGNED_MIN   = -2147483647;
    const INT_SIGNED_MAX   = 2147483647;
    const INT_UNSIGNED_MIN = 0;
    const INT_UNSIGNED_MAX = 4294967295;

    const TINYINT_SIGNED_MIN = -128;
    const TINYINT_SIGNED_MAX = 127;
    const TINYINT_UNSIGNED_MIN = 0;
    const TINYINT_UNSIGNED_MAX = 255;

    const TEXT_MAXLENGTH = 65535;

    const DATETIME_FORMAT = 'yyyy-MM-dd HH:mm:ss';

    /**
     * Returns a singleton instance of the model
     *
     * @param string $className Class name (ignored; required to match signature)
     *
     * @return __CLASS__
     */
    public static function model($className = null)
    {
        $className = empty($className)
                   ? get_called_class()
                   : $className;
        return parent::model($className);
    }

    /**
     * Attempt to auto-guess tableName.  Override this method if the table name
     * is not auto-guessed by this method.
     *
     * @return string
     */
    public function tableName()
    {
        $name = get_called_class();
        $name = Utils::camelToUnderscore($name);
        $name = preg_replace('/_{2,}/', '_', $name);
        $name = preg_replace('/s$/', 'ses', $name);
        $name = preg_replace('/y$/', 'ies', $name);
        return rtrim($name, 's') . 's';
    }


    /**
     * Gather an array of constants and their values that match a certain format.
     *
     * @param string $token       String matching the constant prefix
     * @param string $objectClass If left empty, get_called_class() will be used
     *
     * @example
     * const STATUS_ENABLED  = 'enabled';
     * const STATUS_DISABLED = 'disabled'
     * self::getConstants('STATUS_'); // returns array(
     *      'STATUS_ENABLED'  => 'enabled',
     *      'STATUS_DISABLED' => 'disabled');
     *
     * @return array Key-Value pairs of constants matching $token
     */
    public static function getConstants($token, $objectClass = null)
    {
        if (empty($objectClass))
            $objectClass = function_exists('get_called_class')
                         ? get_called_class()
                         : __CLASS__;

        $tokenLen = strlen($token);

        $reflection   = new ReflectionClass($objectClass); //php built-in
        $allConstants = $reflection->getConstants(); //constants as array

        $tokenConstants = array();
        foreach ($allConstants as $name => $val) {
            if (substr($name, 0, $tokenLen) != $token)
                continue;
            $tokenConstants[$val] = $val;
        }
        return $tokenConstants;
    }

    /**
     * Custom scope to impose a limit on results returned.
     *
     * @param int $limit
     * @return ActiveRecord
     */
    public function limit($limit)
    {
        $this->getDbCriteria()
             ->limit = (int) $limit;

        return $this;
    }

    public function __call($name, $parameters) {
        // Custom RBAC methods prefixed with "can*" (example: canView(User);)
        // default: disallow for any unknown permission (unless admin)
        // @see also WebUser::checkAccess()
        if (preg_match('/^can[A-Z]/', $name)) {
            Yii::app()->log(sprintf('Permission does not exist: %s::%s(s)',
                get_called_class(),
                $name,
                $parameters->primaryKey),
                CLogger::LEVEL_WARNING);
            return false;
        }

        return parent::__call($name, $parameters);
    }
}