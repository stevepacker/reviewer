<?php
Yii::import('ext.YiiMongoDbSuite.test.EMongoDbTestCase');

/**
 * Extension of the built-in DbTestCase provided by the Yii-MongoDB extension
 * to override some features on a global basis.
 */
class MongoDbTestCase extends EMongoDbTestCase
{
	/**
     * Required for Mongo tests, since typical fixture will point to MySQL instead of Mongo
     *
	 * @return CDbFixtureManager the database fixture manager
	 */
	public function getFixtureManager()
	{
        return Yii::app()->getComponent('mongofixture');
	}
    
}