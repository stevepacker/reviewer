<?php

/**
 * Responsible for automatically encoding/decoding data as it's being fetched
 * from the database and saved to the database.
 */
class JsonFieldBehavior extends CActiveRecordBehavior
{
    /**
     * Methods that exist in CJSON
     */
    const CONVERT_ENCODE = 'encode';
    const CONVERT_DECODE = 'decode';

    /**
     * List of fields that are JSON-encoded within the database
     *
     * @var array
     */
    public $fields = array();

    /**
     * Method responsible for looping through all the fields that should be JSON
     * encoded or decoded, and does the work depending on $direction
     *
     * @param string $direction "encode" or "decode"
     *
     * @throws InvalidArgumentException When CJSON does not have a method matching
     *                                  $direction
     */
    protected function convert($direction, $event = null)
    {
        if (!method_exists('CJSON', $direction))
            throw new InvalidArgumentException('CJSON does not have a method: ' . $direction);

        $owner = $this->owner;

        foreach ($this->fields as $field) {
            if ($owner->hasAttribute($field)) {
                $owner->setAttribute($field,
                    CJSON::$direction($owner->getAttribute($field)));
            }
        }
    }

    /**
     * Converts value(s) before validation
     *
     * @param CEvent $event
     */
    public function beforeValidate($event)
    {
        $this->convert(self::CONVERT_ENCODE);

        return parent::beforeValidate($event);
    }

    /**
     * Converts value(s) back after validation.
     *
     * @param CEvent $event
     */
    public function afterValidate($event)
    {
        $this->convert(self::CONVERT_DECODE);

        return parent::afterValidate($event);
    }

    /**
     * Converts an array or object prior to saving to the database.
     *
     * @return boolean
     */
    public function beforeSave($event)
    {
        $this->convert(self::CONVERT_ENCODE);

        return parent::beforeSave($event);
    }

    /**
     * Return fields back to its unserialized format
     *
     * @param CEvent $event
     * @return null
     */
    public function afterSave($event)
    {
        $this->convert(self::CONVERT_DECODE);

        return parent::afterSave($event);
    }

    /**
     * Responsible for decoding anything in $this->extended and converting
     * it to its true form
     *
     * @return null
     */
    public function afterFind($event)
    {
        $this->convert(self::CONVERT_DECODE);

        return parent::afterFind($event);
    }
}

