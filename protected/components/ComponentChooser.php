<?php

/**
 * This class is responsible for picking between available components,
 * attempting to load each in order, and assigning the first successful
 * one to the app. *
 */
class ComponentChooser
{
    public $componentName;

    public $options = array();

    public function init()
    {
        if (empty($this->componentName)) {
            throw new CException('Required attribute "componentName" is not set.');
        }

        if (empty($this->options)) {
            throw new CException('No available options to run through.');
        }

        foreach ($this->options as $key => $value) {
            if (is_numeric($key)) {
                $value = array('class' => $value);
            } else {
                $value['class'] = $key;
            }

            try {
                $component = Yii::createComponent($value);
                Yii::app()->setComponent($this->componentName, $component);
                break;
            } catch (Exception $e) {
                // try the next one
            }
        }

        if (!Yii::app()->hasComponent($this->componentName)) {
            throw new CException('Unable to assign any of the options to component: '
                . $this->componentName);
        }
    }
}
