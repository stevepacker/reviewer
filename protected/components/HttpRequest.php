<?php

class HttpRequest extends CHttpRequest
{
    /**
     * Returns the user IP address.
     * @return string user IP address
     */
    public function getUserHostAddress()
    {
        foreach (array('HTTP_CLIENT_IP', 'HTTP_X_FORWARDED_FOR', 'HTTP_X_FORWARDED', 'REMOTE_ADDR') as $key)
            if (!empty($_SERVER[$key]))
                return $_SERVER[$key];

        return '0.0.0.0';
    }

    /**
     * Checks user_agent of visitor and determines if user is a bot.
     *
     * @link http://neo22s.com/function-to-check-if-visitor-is-a-bot/
     * @return boolean
     */
    public function getUserIsBot()
    {
        $agent   = $this->getUserAgent();
        $botlist = array("Teoma", "alexa", "froogle", "Gigabot", "inktomi",
            "looksmart", "URL_Spider_SQL", "Firefly", "NationalDirectory",
            "Ask Jeeves", "TECNOSEEK", "InfoSeek", "WebFindBot", "girafabot",
            "crawler", "www.galaxy.com", "Googlebot", "Scooter", "Slurp",
            "msnbot", "appie", "FAST", "WebBug", "Spade", "ZyBorg", "rabaz",
            "Baiduspider", "Feedfetcher-Google", "TechnoratiSnoop", "Rankivabot",
            "Mediapartners-Google", "Sogou web spider", "WebAlta Crawler","TweetmemeBot",
            "Butterfly","Twitturls","Me.dium","Twiceler", "Web Link Validator");

        foreach ($botlist as $bot)
        {
            if (false !== strpos($agent, $bot))
                return true;
        }
        return false;   // Not a bot
    }

    /**
     * Identifies if an IP address is a company or internal IP
     *
     * @param string|long $ip IP Address
     * @return boolean
     */
    public function getUserIsInternal($ip = null)
    {
        if (null === $ip)
            $ip = ip2long($this->getUserHostAddress());
        else if (!is_numeric($ip))
            $ip = ip2long($ip);

        switch (true)
        {
            case ($ip == ip2long('4.53.121.74')):   // carlsbad (L3)
            case ($ip == ip2long('72.34.84.234')):  // carlsbad (skyriver)
            case ($ip == ip2long('69.31.123.18')):  // santa monica
            // internal networks {@url http://en.wikipedia.org/wiki/Private_network#Private_IPv4_address_spaces }
            case ($ip >= ip2long('192.168.0.0') && $ip <= ip2long('192.168.255.255')):
            case ($ip >= ip2long('10.0.0.0')    && $ip <= ip2long('10.255.255.255')):
            case ($ip >= ip2long('172.16.0.0')  && $ip <= ip2long('172.31.255.255')):
            case ($ip >= ip2long('127.0.0.0')   && $ip <= ip2long('127.255.255.255')):
                return true;
        }
        return false;
    }
}
