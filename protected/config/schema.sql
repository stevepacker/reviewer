
-- Adminer 3.3.3 MySQL dump

SET NAMES utf8;
SET foreign_key_checks = 0;
SET time_zone = 'SYSTEM';
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `comments`;
CREATE TABLE `comments` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `subject_class` varchar(60) NOT NULL,
  `subject_pk` varchar(255) NOT NULL,
  `comment` text NOT NULL,
  `user_id` int(10) NOT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  `actions` text COMMENT 'JSON Encoded',
  PRIMARY KEY (`id`),
  KEY `user_id_created` (`user_id`,`created`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `project_users`;
CREATE TABLE `project_users` (
  `project_name` varchar(255) CHARACTER SET latin1 NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `role` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`user_id`,`project_name`),
  KEY `project_name` (`project_name`),
  CONSTRAINT `project_users_ibfk_1` FOREIGN KEY (`project_name`) REFERENCES `projects` (`name`),
  CONSTRAINT `project_users_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


DROP TABLE IF EXISTS `projects`;
CREATE TABLE `projects` (
  `name` varchar(255) NOT NULL,
  `type` varchar(15) NOT NULL,
  `uri` text NOT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `reviews`;
CREATE TABLE `reviews` (
  `project` varchar(255) NOT NULL,
  `commit` char(40) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  `status` varchar(30) NOT NULL,
  PRIMARY KEY (`project`,`commit`),
  CONSTRAINT `reviews_ibfk_1` FOREIGN KEY (`project`) REFERENCES `projects` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `settings`;
CREATE TABLE `settings` (
  `key` varchar(255) CHARACTER SET ascii COLLATE ascii_bin NOT NULL,
  `value` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT 'JSON',
  PRIMARY KEY (`key`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `user_aliases`;
CREATE TABLE `user_aliases` (
  `user_id` int(10) unsigned NOT NULL,
  `alias` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`alias`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `user_aliases_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `settings` (`key`, `value`) VALUES ('system.git.path', '/usr/bin/git');

DROP TABLE IF EXISTS `user_forgot_passwords`;
CREATE TABLE `user_forgot_passwords` (
  `user_id` int(10) unsigned NOT NULL,
  `token` char(8) NOT NULL,
  `created` datetime NOT NULL,
  `created_ip` varchar(15) NOT NULL,
  `modified` datetime DEFAULT NULL,
  `modified_ip` varchar(16) DEFAULT NULL,
  PRIMARY KEY (`token`),
  KEY `user_id_created` (`user_id`,`created`),
  KEY `token_created` (`token`,`created`),
  CONSTRAINT `user_forgot_passwords_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `user_sessions`;
CREATE TABLE `user_sessions` (
  `id` char(32) NOT NULL,
  `expire` int(11) DEFAULT NULL,
  `data` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `user_settings`;
CREATE TABLE `user_settings` (
  `user_id` int(10) unsigned NOT NULL,
  `setting` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `value` text COLLATE utf8_unicode_ci COMMENT 'JSON-encoded',
  UNIQUE KEY `setting_user_id` (`setting`,`user_id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `user_settings_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `display_name` varchar(60) CHARACTER SET utf8 NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` char(40) COLLATE utf8_unicode_ci NOT NULL,
  `salt` char(10) COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL,
  `roles` text COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `updated` datetime DEFAULT NULL,
  `last_login` datetime DEFAULT NULL,
  `last_ip` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


DROP TABLE IF EXISTS `yiicache`;
CREATE TABLE `yiicache` (
  `id` char(128) NOT NULL,
  `expire` int(11) DEFAULT NULL,
  `value` longblob,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;


-- 2012-06-20 22:54:11
