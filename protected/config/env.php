<?php
/**
 * This file should be called in index.php, not config.php so that the constants
 * exist before init'ing the web application
 */
defined('DS') || define('DS', DIRECTORY_SEPARATOR);

if (defined('__ENV')) {
    // do nothing
} elseif (!empty($_SERVER['ENVIRONMENT'])) {
    define('__ENV', $_SERVER['ENVIRONMENT']);
} elseif (!empty($_ENV['ENVIRONMENT'])) {
    define('__ENV', $_ENV['ENVIRONMENT']);
} else {
    // environment autodetect
    switch (gethostname())
    {
        default:
            define('__ENV', 'development');
            break;
    }
}