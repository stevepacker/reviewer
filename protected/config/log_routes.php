<?php

// define logging option based on environment mode
$logging_routes = array();
$logging_routes[] = array(
    'class'       => 'CFileLogRoute',
    'categories'  => 'task',
    'logFile'     => 'task.log',
    'maxLogFiles' => 10, // Due to shared distributed system, this needs to be high
    // as logs may be rotated by each node
    'maxFileSize' => 102400, // 100M
);

switch (__ENV)
{
    case 'development':
        // intential passthru
    case 'test':
        $logging_routes[] = array(
            'class'         => 'CFileLogRoute',
            'levels'        => 'trace',
            'logFile'       => 'trace.log',
        );
        $logging_routes[] = array(
            'class'         => 'CFileLogRoute',
            'levels'        => 'info',
            'logFile'       => 'info.log',
        );
        $logging_routes[] = array(
            'class'      => 'CFileLogRoute',
            'levels'     => 'profile',
            'logFile'    => 'profile.log',
        );
        if (php_sapi_name() !== 'cli')
            $logging_routes[] = array(
                'class'      => 'application.extensions.pqp.PQPLogRoute',
                'categories' => 'application.extensions.pqp.PQPLogRoute',
            );
        // intentional passthru
    case 'qa':
    case 'ci':
    case 'production':
    default:
        $logging_routes[] = array(
            'class'  => 'CFileLogRoute',
            'levels' => 'warning',
            'logFile'    => 'warning.log',
        );
        $logging_routes[] = array(
            'class'  => 'CFileLogRoute',
            'levels' => 'error',
            'logFile'    => 'error.log',
        );
        break;
}