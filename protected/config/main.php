<?php

require_once 'env.php';
require_once 'db.config.php';
require_once 'log_routes.php';


Yii::setPathOfAlias('ext', realpath( dirname(__FILE__) . DS . '../extensions/'));


// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
    'basePath' => realpath(dirname(__FILE__) . DS . '..'),
    'name'     => 'production' === __ENV
                ? 'Reviewer -- Code Review Interface'
                : sprintf('Reviewer -- Code Review Interface [%s]', strtoupper(__ENV)),

    // preloading 'log' component
    'preload'=>array('log', 'autoloader', 'bootstrap', 'cacheChooser'),

    // autoloading model and component classes
    // Note: the items are added to the include path in reverse order
    'import'=>array(
        'application.models.*',
        'ext.mail.YiiMailMessage',
        'application.components.*',
    ),

    'modules'=>array(),

    // application components
    'components' => array(
        'autoloader' => array(
            'class' => 'application.components.AutoLoader',
            // Note: the directories are searched in the order given
            'include_parent_dirs' => array(
                'models',
                'components',
                'extensions/YiiGit',
                'controllers',
                'tests',
            ),
        ),

        'user' => array(
            // enable cookie-based authentication
            'class'           => 'WebUser',
            'allowAutoLogin'  => true,
            'loginUrl'        => array('login/'),
        ),

        'urlManager'=>array(
            'urlFormat'      => 'path',
            // include index.php if it starts with it
            'showScriptName' => false,//empty($_SERVER['REQUEST_URI']) || false !== strpos($_SERVER['REQUEST_URI'], 'index.php'),
            'rules'          => array(
                'login'                                         => 'site/login', // dedicated login route
                '<controller:\w+>/<action:\w+>/<id:[\-\w\ ]+>'    => '<controller>/<action>',
                '<controller:\w+>/<action:\w+>'                 => '<controller>/<action>',
            ),
        ),

        'request' => array(
            'class' => 'HttpRequest',
        ),

        'bootstrap' => array(
            'class' => 'ext.bootstrap.components.Bootstrap'
        ),

        // using yiimongodb suite: http://www.yiiframework.com/extension/yiimongodbsuite/
//        'mongodb' => array(
//            'class'            => 'MMongoDB',
//            'connectionString' => 'mongodb://' . __MONGODB_HOST,
//            'dbName'           => __MONGODB_DB,
//            'fsyncFlag'        => true,  //make sure all writes to the database are safely stored to disk
//            'safeFlag'         => true,  //retrieve status of all write operations, and check if everything went OK
//            'useCursor'        => false, //return EMongoCursor instead of raw pre-populated arrays, form findAll* methods
//            'persistentConnection' => false,//__MONGODB_REPLICASET,
//            'replicaSet'           => __MONGODB_REPLICASET,
//        ),

        'db' => array(
            'class'                 => 'DbConnectionMan',
            'connectionString'      => 'mysql:host=' . __MYSQL_HOST . ';dbname=' . __MYSQL_DB,
            'emulatePrepare'        => true,
            'username'              => __MYSQL_USER,
            'password'              => __MYSQL_PASSWD,
            'charset'               => 'utf8',
            'enableProfiling'       => 'development' === __ENV,
            'enableParamLogging'    => 'development' === __ENV,
            'slaves'                => CJSON::decode(__MYSQL_SLAVES),
            'schemaCacheID'         => 'cache',
            'schemaCachingDuration' => 86400,
            // CacheDirector uses 'settings' model to pull info such as 'cache.version'.
            'schemaCachingExclude'  => array('settings'), // list of tables of not cache schema
        ),

        'cacheChooser' => array(
            'class'         => 'ComponentChooser',
            'componentName' => 'cache',
            'options'       => array(
//                'CApcCache',
//                'CMemCache',
//                'CDbCache' => array(
//                   'connectionID'   => 'db',
//                   'cacheTableName' => 'yiicache',
//                ),
                'CFileCache' => array(
                    'cacheFileSuffix' => '.txt',
                ),
                'CDummmyCache',
            ),
        ),

        'session' => array(
            'class'            => 'CDbHttpSession',
            'connectionID'     => 'db',
            'sessionTableName' => 'user_sessions',
        ),

        //using sendmail
        'mail' => array(
            'class' => 'ext.mail.YiiMail',
            'transportType' => 'php',
            'viewPath' => 'application.views.mail',
            'logging' => true,
            'dryRun' => false,//('production' !== __ENV),
        ),

        'clientScript'=>array(
            'class' => 'ClientScript',
            'scriptMap' => array(
                'jquery.js'=>false,
                'jquery.min.js'=>false,
            ),
            'coreScriptPosition' => CClientScript::POS_BEGIN,
        ),

        'format' => array(
            'class' => 'Formatter',
            'dateFormat'     => 'F j, Y',
            'datetimeFormat' => 'F j, Y -- g:ia',
        ),

//        'errorHandler'=>array(
//            // use 'site/error' action to display errors
//            'errorAction'=>'site/error',
//        ),

        'log'=>array(
            'class'  => 'CLogRouter',
            'routes' => $logging_routes,
        ),
    ),

    // application-level parameters that can be accessed
    // using Yii::app()->params['paramName']
    'params' => array(
        // this is used in contact page
        'adminEmail' => 'no-reply@stevepacker.com',

        // this is used to hash passwords; change only if you want to invalidate all passwords
        'siteSalt'   => 'L@#]X-Q.cnQ=WjGX',
    ),

);
