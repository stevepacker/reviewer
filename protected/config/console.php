<?php

// Override to contact remote databases for CLI operations (i.e. reports)
//define('__ENV', 'qa');

$config = include dirname(__FILE__) . DIRECTORY_SEPARATOR . 'main.php';

// PQP extension breaks CLI operations, so remove from preloader
foreach ($config['preload'] as $key => $value)
{
    if ('log' === $value)
        unset($config['preload'][$key]);
}

return $config;