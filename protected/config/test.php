<?php

defined('__ENV') || define('__ENV', 'test');

return CMap::mergeArray(
    require(dirname(__FILE__).'/main.php'),
    array(
        'components'=>array(
            'fixture'=>array(
                'class'=>'DbFixtureManager',
            ),
            'mongofixture' => array(
                'class' => 'ext.YiiMongoDbSuite.test.EMongoDbFixtureManager',
            ),
        ),
    )
);
