<?php
switch (__ENV) 
{
    case 'production':
        
    case 'qa':
    
    case 'ci':
    
    case 'test':
    
    case 'development':
        
    default:
        //MYSQL server information
        defined('__MYSQL_HOST')         || define('__MYSQL_HOST',       'localhost');
        defined('__MYSQL_USER')         || define('__MYSQL_USER',       'reviewer');
        defined('__MYSQL_PASSWD')       || define('__MYSQL_PASSWD',     'reviewer');
        defined('__MYSQL_DB')           || define('__MYSQL_DB',         'reviewer');
        defined('__MYSQL_SLAVES')       || define('__MYSQL_SLAVES',     CJSON::encode(array()));

        //MONGODB server information
        defined('__MONGODB_HOST')       || define('__MONGODB_HOST',        'localhost');
        defined('__MONGODB_USER')       || define('__MONGODB_USER',        '');
        defined('__MONGODB_PASSWD')     || define('__MONGODB_PASSWD',      '');
        defined('__MONGODB_DB')         || define('__MONGODB_DB',          'reviewer');
        defined('__MONGODB_PERSIST')    || define('__MONGODB_PERSIST',     'reviewer_SMZg22Sx'); // set to any random string to persist
        defined('__MONGODB_REPLICASET') || define('__MONGODB_REPLICASET',  '');

        //Memcache Config
        defined('__MEMCACHE_SERVERS')   || define('__MEMCACHE_SERVERS', CJSON::encode(array()));
                
        break;
}