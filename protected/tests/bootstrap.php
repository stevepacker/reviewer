<?php

// change the following paths if necessary
$yiit = __DIR__.'/../yii/yiit.php';
$config = __DIR__.'/../config/test.php';

require_once($yiit);
require_once(__DIR__.'/WebTestCase.php');

Yii::createWebApplication($config);

// Register shutdown function to flush logs
register_shutdown_function(function() {
     Yii::app()->end();
});