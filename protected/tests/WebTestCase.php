<?php

/**
 * The base class for functional test cases.
 * In this class, we set the base URL for the test application.
 * We also provide some common methods to be used by concrete test classes.
 * 
 * @see Element Locators http://release.seleniumhq.org/selenium-remote-control/0.9.2/doc/java/com/thoughtworks/selenium/Selenium.html#locators
 */
class WebTestCase extends CWebTestCase
{

    /**
     * Set the URL for PHPUnit code coverage handling
     * @var string
     */
    protected $coverageScriptUrl = 'http://testccm.business.com/phpunit_coverage.php';

	/**
	 * Sets up before each test method runs.
	 * This mainly sets the base URL for the test application.
	 */
	protected function setUp()
	{
		parent::setUp();

        /*$subdomain = 'localccm';
        // hostname will look like: labqci01, so remove number so it can run on any CI box
        $hostname  = preg_replace('/[\d]+$/', '', gethostname());
        if (in_array($hostname, array('labqci', 'ciorigin', 'ciccm')))
            $subdomain = 'ciccm';*/

		$this->setBrowserUrl("http://testccm.business.com/");

		// Add support for loading both MySQL and Mongo fixtures
		if (isset($this->mongoFixtures) && is_array($this->mongoFixtures))
		    Yii::app()->getComponent('mongofixture')->load($this->mongoFixtures);
	}

	/**
	 * Simulates a login of a given user type
	 *
	 * @see login
	 * @param string $type The type of user to login as
	 * @return bool If login was successful
	 */
	protected function loginAsUserType($type = 'advertiser')
	{
		static $testAccounts;
		if (null == $testAccounts)
		{
			$accountsIni = Yii::app()->basePath . DS . 'config' . DS . 'test.ini';
			$testAccounts = parse_ini_file($accountsIni, true, INI_SCANNER_RAW);
		}

		if (isset($testAccounts[$type]))
		{
			list($username, $password) = array_values($testAccounts[$type]);
		}
		else
		{
			self::fail('Invalid user type for login');
			return false;
		}

		return $this->login($username, $password);
	}

	/**
	 * Performs the actions necessary to login with the given credentials
	 * @param string $username User to login as
	 * @param string $password Password for the given user
	 * @return bool If login was successful
	 */
	protected function login($username, $password)
	{
		$this->open('');
		$logoutText = 'Log Out';

        // ensure the user is logged out
        if ($this->isTextPresent($logoutText))
            $this->open('/site/logout');

		$this->open('/login');
		$this->type('id=LoginForm_username', $username);
		$this->type('id=LoginForm_password', $password);
		$this->clickAndWait('name=login-box-submit');

		$loggedIn = $this->isTextPresent($logoutText);

		if (! $loggedIn)
		    self::fail('Attempt to login as a user was unsuccessful');

		return $loggedIn;
	}

	/**
	 * Loads the session of the current logged in user.
	 *
	 * Note: The session should be closed ({@link CHttpSession::close}
	 * before another request to the front end. Otherwise, when the close
	 * occurs to get fresh data, you will have inconsistent data state.
	 *
	 * @return CHttpSession The Configured Session Handler
	 */
	protected function getCurrentUserSession()
	{
	    $sessionHandler = Yii::app()->session;

	    // Can only have 1 open session. Close any existing sessions first
	    if ($sessionHandler->getIsStarted()) {
	        $sessionHandler->close();
	    }

	    // Get the session ID from Selenium
	    $cookieName = $sessionHandler->getSessionName();
        $sessionId = $this->getCookieByName($cookieName);

        // Get the session data
        $sessionHandler->setSessionID($sessionId);
        $sessionHandler->open();

        return $sessionHandler;
	}

}
