<?php
class SiteController extends Controller
{
    /**
     * Used to prevent bruteforce attacks on forgot password
     */
    const FORGOT_HOURLY_RETRIES = 0;

    public function actionIndex()
    {
        $user = $this->view->user     = User::getUser();
        $this->view->projects = $user->projects;
    }

    public function actionError()
    {
        if (($error = Yii::app()->errorHandler->error))
        {
            if (Yii::app()->request->isAjaxRequest)
                echo $error['message'];
            else
                $this->view->error = $error;
        }
    }


    /**
     * Displays the login page
     */
    public function actionLogin()
    {
        $model = new LoginForm;

        // if it is ajax validation request
        if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
        {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }

        // collect user input data
        if(isset($_POST['LoginForm']))
        {
            $model->attributes=$_POST['LoginForm'];
            // validate user input and redirect to the previous page if valid
            if($model->validate() && $model->login())
                $this->redirect(Yii::app()->user->returnUrl);
        }
        // display the login form
        $this->view->model = $model;
    }

    /**
     * Logs out the current user and redirect to homepage.
     */
    public function actionLogout()
    {
        Yii::app()->user->logout();
        $this->redirect(Yii::app()->homeUrl);
    }

    public function actionForgotPassword()
    {
        $model = $this->view->model = new User_ForgotPasswordForm;
        if (!$_POST)
            return;

        // prevent brute-force attacks
        $cache_key        = 'forgot_password_' . Yii::app()->request->getUserHostAddress();
        $request_tries    = (int) Yii::app()->cache->get($cache_key);
        Yii::app()->cache->set($cache_key, ++$request_tries, 60*60);

        if (self::FORGOT_HOURLY_RETRIES && $request_tries > self::FORGOT_HOURLY_RETRIES)
            throw new CHttpException(403,
                'We have seen too many forgot password requests from you within
                 the last hour; please wait 60 minutes  before trying again.
                 Thank you.');

        $model->attributes = $_POST['User_ForgotPasswordForm'];
        if ($model->validate()) {
            $user = User::getUser($model->email);
            if (!$user) {
                $this->view->login_found = false;
                $this->view->response    = 'We were unable to identify an
                    account with that email address.  Please check your records,
                    or contact support for assistance.';
                return;
            }
        }

        $transaction = Yii::app()->db->beginTransaction();
        try
        {
            // first check for a pre-existing token
            $criteria = new CDbCriteria;
            $criteria->condition = '
                user_id = :user_id
                AND modified_date IS NULL
                AND creation_date > DATE_SUB(NOW(), INTERVAL 4 HOUR)';
            $criteria->params[':user_id'] = $user->id;
            $token = User_ForgotPassword::model()->unexpired()->userScope($user->id)->find();

            if (is_null($token))
            {
                // create a new token
                $token = new User_ForgotPassword;
                $token->user_id = $user->id;
                $token->token   = substr(md5($user->salt . microtime(true) . rand(0,99) . rand(0,99)), -8);
                $token->save();
            }

            $mail = new YiiMailMessage;
            $mail->view    = 'forgotPassword';
            $mail->from    = Yii::app()->params['adminEmail'];
            $mail->subject = Yii::app()->name . ' -- Password Reset Request';
            $mail->addTo($user->email);
            $mail->setBody(array(
                'username' => $user->email,
                'url'      => $this->createAbsoluteUrl('site/resetPassword', array('id' => $token->token)),
            ), 'text/html');
            Yii::app()->mail->send($mail);

            $transaction->commit();

            Yii::app()->user->setFlash('success', 'We have received your request.
                You should recieve an email in the next 5-10 minutes with instructions
                to create a new password.  If you continue to have trouble, please
                contact support for further assistance.');
        }
        catch (Exception $e)
        {
            $transaction->rollback();
            throw $e;
        }
    }

    public function actionResetPassword($id)
    {
        $token = User_ForgotPassword::model()->unexpired()->tokenScope($id)->find();

        if (!$token) {
            Yii::app()->user->setFlash('error', 'Your link either expired, or
                we were unable to find it; please try again.');
            $this->redirect(array('site/forgotPassword'));
        }

        // invalidate the token
        $token->save();

        $user = $token->user;
        if (!$user) {
            Yii::app()->user->setFlash('error', 'Your link either expired, or
                we were unable to find your username; please try again.');
            $this->redirect(array('site/forgotPassword'));
        }

        // force login
        $identity = new UserIdentity($user->email, '');
        $identity->forceAuthenticate();
        Yii::app()->user->login($identity);

        // set flash message to alert user what's happening
        Yii::app()->user->setFlash('info', 'Please change your password below.');

        $this->redirect(array('user/update', 'id' => $user->id));
    }
}