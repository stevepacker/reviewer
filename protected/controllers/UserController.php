<?php

class UserController extends Controller
{
    public $defaultAction = 'list';

    public function actionList()
    {
        $this->view->users = User::model();
    }

    public function actionView($id = null)
    {
        if (null === $id) {
            $id = Yii::app()->user->id;
        }

        $user = $this->loadModel($id);

        $this->view->comments = Comment::model()
                ->newestFirst()
                ->ofUser($user)
                ->limit(3)
                ->findAll();
    }

    public function actionCreate()
    {
        $user = $this->view->user = $this->_update(new User);
        if (!$user->isNewRecord && !$user->hasErrors()) {
            Yii::app()->user->setFlash('success', 'User has been created.');
            $this->redirect(array('user/view', 'id' => $user->primaryKey));
        }
    }

    public function actionUpdate($id = null)
    {
        if (empty($id))
            $id = Yii::app()->user->id;
        
        $user = $this->loadModel($id);

        if (empty($_POST))
            return;

        if (!empty($_POST['User']['password']))
            $user->scenario = 'updatePassword';

        $user = $this->_update($user);
        if (!$user->hasErrors()) {
            Yii::app()->user->setFlash('success', 'Your changes have been saved.');
            $this->redirect(array('user/view', 'id' => $id), 303);
        } else {
            Yii::app()->user->setFlash('error', print_r($user->errors, true));
        }


    }

    /**
     * Updates a model's attributes from $_POST and saves it.
     *
     * @param  User $model
     * @return User
     */
    private function _update($model)
    {
        if (!empty($_POST[get_class($model)])) {
            $model->attributes = $_POST[get_class($model)];
            $model->save();
            // make sure there's always at least one admin
            if (1 == User::model()->count()
                && (!is_array($model->roles)
                    || !in_array(User::ROLE_ADMIN, $model->roles))) {
                Yii::app()->user->setFlash('info',
                    'Admin role was forced onto this user so that there is at least one Admin on the site.');
                $model->roles = array(User::ROLE_ADMIN);
                $model->save();
            }
        }

        return $model;
    }

    public function actionDelete($id)
    {
        if (!Yii::app()->request->isPostRequest)
            throw new CHttpException(400);

        $user = $this->loadModel($id, array('assertPost' => true));
        if (!$user)
            throw new CHttpException(404);
        else
            $user->delete();

        $this->redirect(array('user/list'), 303);
    }

    public function actionAddAlias($id = null)
    {
        if (null === $id)
            $id = Yii::app()->user->id;

        $user = $this->loadModel($id, array(
            'assertPost' => true,
            'assertRole' => 'user.editAlias',
        ));

        $alias          = new User_Alias;
        $alias->alias   = Arr::getDot($_REQUEST, array('alias', 'User_Alias.alias'));
        $alias->user_id = $user->id;
        if (!$alias->save())
            throw new CHttpException(500, 'Save failure: ' . print_r($alias->errors, true));

        $this->redirect(array('user/view', 'id' => $id), true, 303);
    }

    public function actionRemoveAlias($alias, $id = null)
    {
        if (null === $id)
            $id = Yii::app()->user->id;

        $user = $this->loadModel($id, array(
            'assertPost' => true,
            'assertRole' => 'user.editAlias',
        ));

        $alias = User_Alias::model()->findByPk($alias);
        if (!$alias)
            throw new CHttpException(404);

        $alias->delete();
        $this->redirect(array('user/view', 'id' => $user->id), true, 303);
    }

    public function actionAddProject($project, $id = null)
    {
        if (null === $id)
            $id = Yii::app()->user->id;

        $user = $this->loadModel($id, array(
            'assertPost' => true,
            'assertRole' => 'user.editProject',
        ));

        $map  = new Project_User;
        $map->user_id = $user->id;
        $map->project_name = $project;
        if (!$map->save()) {
            Yii::app()->user->setFlash('error', Html::dl_errors($map));
        } else {
            Yii::app()->user->setFlash('success', 'Successfully added to project');
        }
        $this->redirect(array('user/view', 'id' => $id), true, 303);
    }
    public function actionRemoveProject($id, $project)
    {

    }

    public function actionLoginAs($id)
    {
        Yii::app()->user->requireAccess('admin');

        $user     = $this->loadModel($id);
        $identity = new UserIdentity($user->email, $user->password);
        $identity->forceAuthenticate($user);
        Yii::app()->user->login($identity);
        Yii::app()->user->setFlash('success', 'Login as ' . $user->display_name . ' successful.');
        $this->redirect(Yii::app()->homeUrl);
    }

    public function actionSettings($id = null)
    {
        $user = $this->view->user      = User::getUser($id);
        $form = $this->view->formModel = User_SettingForm::loadFromUser($user);

        if ($user && !empty($_POST['User_SettingForm'])) {
            $form->attributes = $_POST['User_SettingForm'];
            if ($form->validate()) {
                User_Setting::model()->deleteAllByAttributes(array(
                    'user_id' => $user->id,
                ));
                foreach ($form->attributes as $name => $value) {
                    $setting = new User_Setting;
                    $setting->user_id = $user->id;
                    $setting->setting = $name;
                    $setting->value   = $value;
                    $setting->save();
                }
                Yii::app()->user->setFlash('success', 'Settings have been saved.');
                $this->redirect(array('user/view', 'id' => $user->id));
            }
        }
    }
}