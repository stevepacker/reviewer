<?php
class ReviewController extends Controller
{
    public function actionFile($project, $filename, $commit)
    {
        $project = Project::model()->findByPk($project);
        if (!$project)
            throw new CHttpException('404', 'Unable to find project.');

        // @todo abstract to support SVN or GIT
        $commit = $project->repo->getCommit($commit);
        if (!($commit instanceof AGitCommit))
            throw new CHttpExteption('404', 'Unable to find this commit');

        $review = Review::get($commit->hash, $project->name);

        $this->view->project      = $project;
        $this->view->commit       = $commit;
        $this->view->review       = $review;
        $this->view->filename     = $filename;
        $this->view->diffs        = $review->getFileDiff($filename);

        echo $this->renderPartial('file', $this->view->toArray());
        Yii::app()->end();
    }

    /**
     * View the review information for the given commit.
     *
     * If callback is defined, JavaScript will be generated with the given callback
     * function called with the basic review information. Useful for JSON-P requests.
     *
     * @param string $commit   Commit hash
     * @param string $project  Project name
     * @param string $callback Name of JS function to callback with review status.
     */
    public function actionView($commit, $project, $callback = false)
    {
        $project = Project::model()->findByPk($project);
        if (!$project)
            throw new CHttpException('404', 'Unable to find project.');

        // @todo abstract to support SVN or GIT
        $commit = $project->repo->getCommit($commit);
        if (!($commit instanceof AGitCommit))
            throw new CHttpExteption('404', 'Unable to find this commit');

        $review = Review::get($commit->hash, $project->name);
        // Handle JSONP requests
        if ($callback) {
             echo $callback . '(' . CJSON::encode(
                 array(
                     'commit'  => $commit->hash,
                     'project' => $project->name,
                     'status'  => $review->status,
                )
            ) . ');';
            Yii::app()->end();
        }

        $reviewForm = new ReviewForm;
        $reviewForm->commit  = $commit->hash;
        $reviewForm->project = $project->name;
        $reviewForm->status  = $review->status;

        if (!empty($_POST[get_class($reviewForm)])) {
            $transact = $project->getDbConnection()->beginTransaction();
            try {
                $reviewForm->attributes = $_POST[get_class($reviewForm)];
                if ($reviewForm->save()) {
                    $transact->commit();
                    Yii::app()->user->setFlash('success',
                        'Your comment has been saved.');
                    $this->redirect(array(
                        'review/view',
                        'commit'  => $commit->hash,
                        'project' => $project->name,
                        '#' => 'comments'));
                }

            } catch (Exception $e) {
                $transact->rollback();
                throw $e;
            }
        }

        $this->view->project      = $project;
        $this->view->review       = $review;
        $this->view->comments     = $this->view->review->comments;
        $this->view->commit       = $commit;
        $this->view->commentModel = $reviewForm;
    }

    public function actionViewers($commit, $includeSelf = false)
    {
        $viewers = Review_Viewer::get($commit);
        $viewers->addViewer(Yii::app()->user->id);

        $users = array();
        foreach ($viewers->users as $user) {
            // don't include self
            if (Yii::app()->user->id != $user->id || $includeSelf) {
                $users[] = array(
                    'name'    => $user->display_name,
                    'id'      => $user->id,
                    'profile' => $this->createUrl('user/view', array('id' => $user->id)),
                );
            }
        }

        $this->_sendEventStream($users);
        Yii::app()->end();
    }

    private function _sendEventStream($content, $jsonEncode = true)
    {
        if (!headers_sent()) {
            header('Content-Type: text/event-stream');
            header('Cache-Control: no-cache');
            header('Access-Control-Allow-Origin: ' . @$_SERVER['HTTP_ORIGIN']);
            //header('Access-Control-Allow-Credentials: true');

            // prevent bufferring
            if (function_exists('apache_setenv')) {
                @apache_setenv('no-gzip', 1);
            }
            @ini_set('zlib.output_compression', 0);
            @ini_set('implicit_flush', 1);
            for ($i = 0; $i < ob_get_level(); $i++) { ob_end_flush(); }
            ob_implicit_flush(1);
        }

        // getting last-event-id from POST or from http headers
        $postData = @file_get_contents('php://input');
        parse_str($postData, $tmp);
        if (isset($tmp['Last-Event-ID'])) {
            $lastEventId = $tmp['Last-Event-ID'];
        } else {
            $lastEventId = @$_SERVER["HTTP_LAST_EVENT_ID"];
        }

        // 2kb padding for IE
        echo ':' . str_repeat(' ', 2048) . "\n";

        // event-stream
        if ($jsonEncode) {
            $content = CJSON::encode($content);
        }
        echo "id: $i\n";
        echo "data: $content\n\n";
    }
}
