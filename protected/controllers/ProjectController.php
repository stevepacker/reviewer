<?php

class ProjectController extends Controller
{
    public $defaultAction = 'list';

    public function actionList()
    {
        $this->view->projects = new CActiveDataProvider('Project');
    }

    public function actionCreate()
    {
        $model = new Project;
        $this->_update($model);
        $this->view->model = $model;
    }

    public function actionDelete($id)
    {
        $model = Project::model()->findByPk($id);
        if (!$model)
            throw new CHttpException(404, 'Not Found');

//        if (!Yii::app()->request->isPostRequest)
//            throw new CHttpException(400, 'Invalid request');

        $model->delete();
        Yii::app()->user->setFlash('success', 'Project has been successfully deleted.');
        $this->redirect(array('project/list'));
    }

    public function actionUpdate($id)
    {
        $model = Project::model()->findByPk($id);
        if (!$model)
            throw new CHttpException(404, 'Not Found');

        $this->_update($model);
        $this->view->model = $model;
    }

    private function _update(&$model)
    {
        /* @var $model ActiveRecord */
        if (!empty($_POST['Project']))
        {
            $is_new = $model->isNewRecord;
            $model->attributes = $_POST['Project'];
            if ($model->save())
            {
                $message = $is_new
                         ? 'Project Created'
                         : 'Project Changes Saved';
                $route   = $is_new
                         ? array('project/view')
                         : array('project/edit');
                $route['id'] = $model->name;

                Yii::app()->user->setFlash('success', $message);
                $this->redirect($route, 303);
            }
        }
        $this->view->model = $model;
    }

    public function actionView($id, $limit=5, $offset=0, $since=null, $status=null)
    {
	ini_set('memory_limit', '500M');

        $project = Project::model()->findByPk($id);
        if (!$project) {
            throw new CHttpException(404, 'Project not found.');
        }
        $branch  = $this->view->branch
                 = Yii::app()->session->get("branch.$id", 'master');

        if ($limit < 0)
            $limit = 0;
        if ($limit > 30)
            $limit = 30;

        if (isset($project->repo->branches[$branch])) {
            $branch      = $project->repo->branches[$branch];
            $commit_list = $branch->getCommits($_GET);
            $commits     = array();
            $since       = $since
                         ? $since
                         : Yii::app()->user->getFlash('since');

            $i = 0;
            foreach ($commit_list as $commit) {
                // ignore merges
                if (empty($commit->files) || $commit->files === array(''))
                    continue;

                // stop at $since hash, if set
                if ($since && $commit->hash == $since)
                    break;

                // limit results unless $since is set
                if (!$since && count($commits) >= $limit)
                    break;

                if ($status) {
                    // determine status of commit
                    $review = Review::get($commit->hash, $project->name);
                    if ($review->status != $status)
                        continue;
                }

                $i++;
                if ($i <= $offset) {
                    continue;
                }

                $commits[] = $commit;
            }
            if (!empty($commits) && $since)
                Yii::app()->user->setFlash('since', $since);
//var_dump(count($commits));exit;
        }
        $this->view->model   = $project;
        $this->view->commits = $commits;
    }

    public function actionSetBranch($id, $branch = 'master')
    {
        Yii::app()->session["branch.$id"] = $branch;
        $this->redirect(array('project/view', 'id' => $id));
    }

    public function actionUpdateRepo($id)
    {
        $project = Project::model()->findByPk($id);

        $project->repo->pull();
        $project->updated = date('Y-m-d H:i:s');
        $project->save();

        Yii::app()->user->setFlash('success', 'Latest code pulled.');
        $this->redirect(array('project/view', 'id' => $id));
    }
    
    public function actionBookmark($id, $commit)
    {
        $project = Project::model()->findByPk($id);
        if (!$project) {
            throw new CHttpException(404, 'Unable to find project.');
        }
        
        $commit = trim($commit);
        if (empty($commit)) {
            throw new CHttpException(400);
        }
        
        $project->setBookmark($commit);
        
        Yii::app()->user->setFlash('success', 'Bookmarked.');
        $this->redirect(array('project/view', 'id' => $id));
    }
}