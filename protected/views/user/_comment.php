    <div style="font-size: 11px;"><?php echo Yii::app()->format->datetime($comment->created) ?></div>
    <div><a style="font-size: 11px;"><?php echo CHtml::link("{$comment->subject->project}: {$comment->subject->commit}", array(
        'review/view',
        'project' => $comment->subject->project,
        'commit'  => $comment->subject->commit
    )) ?></a></div>
    <div class="well well-small">
        <?php echo CHtml::encode($comment->comment) ?>
    </div>
    <?php if ($comment->actions): ?>
    <?php foreach ($comment->actions as $field => $values): ?>
        <em>
            <i class="icon-flag"></i>
            <?php echo sprintf('%s changed from "%s" to "%s"',
                    ucfirst(CHtml::encode($field)),
                    CHtml::encode($values['old']),
                    CHtml::encode($values['new'])) ?>
        </em>
    <?php endforeach ?>
    <?php endif ?>