<div class="form">
<?php /** @var BootActiveForm $form */
$form  = $this->beginWidget('bootstrap.widgets.BootActiveForm', array(
    'type'        => 'horizontal',
    'htmlOptions' => array('class'=>'well'),
)); ?>

<?php echo $form->checkboxRow($formModel, 'email_myCodeReviewed'); ?>
<?php echo $form->checkboxRow($formModel, 'email_myReviewCommented'); ?>


<div class="form-actions">
    <?php $this->widget('bootstrap.widgets.BootButton', array(
        'buttonType'=>'submit',
        'type'=>'primary',
        'icon'=>'ok white',
        'label'=>$user->isNewRecord ? 'Create' : 'Save')); ?>
    <?php $this->widget('bootstrap.widgets.BootButton', array(
        'buttonType'=>'reset',
        'icon'=>'remove',
        'label'=>'Reset')); ?>
</div>
<?php $this->endWidget(); ?>
</div>