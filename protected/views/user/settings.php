<?php
$this->breadcrumbs['Users'] = array('user/list');
$this->breadcrumbs[CHtml::encode($user->email)] = array('user/view', 'id' => $user->primaryKey);
$this->breadcrumbs[] = 'Settings';
?>

<h1>Settings:</h1>

<?php include '_settings_form.php'; ?>