<?php $this->breadcrumbs[] = 'Users' ?>

<h1>
    Users:
    <?php echo CHtml::link('&plus; new', array('user/create'), array('class' => 'btn btn-mini btn-primary')) ?>
</h1>
<?php $this->widget('bootstrap.widgets.BootGridView', array(
    'dataProvider'=>$users->search(),
    'filter' => $users,
    'template'=>"{items}\n{pager}",
    'columns'=>array(
        'display_name',
        'email',
//        'roles',
//        array(
//            'class' => 'CLinkColumn',
//            'id'=>'email',
//            'labelExpression' => 'CHtml::encode($data->email)',
//            'urlExpression'   => 'array("user/view", "id" => $data->primaryKey)',
//            'header'=>'Email',
//        ),
        'status:boolean',
        'last_login:datetime',
        array(
            'class'=>'bootstrap.widgets.BootButtonColumn',
            'htmlOptions'=>array('style'=>'width: 50px'),
        ),
    ),
)); ?>