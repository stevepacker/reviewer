<?php
$this->breadcrumbs['Users'] = array('user/list');
$this->breadcrumbs[] = CHtml::encode($user->email);
?>

<div class="row">
    <div class="span10">
        <?php $this->widget('bootstrap.widgets.BootDetailView', array(
            'data'=>$user,
            'attributes' => array(
                'display_name',
                'email',
                'status:boolean',
                'created:datetime',
                'updated:datetime',
                'last_login:datetime',
                'last_ip',
                array(
                    'label' => 'Roles',
                    'value' => implode(", ", (array) $user->roles),
                ),
                array(
                    'label' => 'Projects',
                    'value' => CHtml::encode(implode(", ", (array) CHtml::listData($user->projects, 'name', 'name'))),
                ),
            ),
        )); ?>
    </div>
    <div class="span2">
        <?php echo Html::gravitar($user->email, array(
            'default'     => 'mm',
            'size'        => 130,
            'htmlOptions' => array(
                'class' => 'thumbnail',
            ),
        )) ?>
        <?php if (Yii::app()->user->id == $user->id): ?>
        <?php echo CHtml::link('Change Gravatar', 'http://www.gravatar.com') ?>
        <?php endif ?>
    </div>
</div>

<div class="row">
    <div class="span12">
        <?php $this->widget('bootstrap.widgets.BootMenu', array(
            'type'  => 'pills', // '', 'tabs', 'pills' (or 'list')
            'items' => array(
                array(
                    'label'   => 'Settings',
                    'url'     => array('user/settings', 'id' => $user->primaryKey),
                    'icon'    => 'cog',
                    'visible' => Yii::app()->user->checkAccess('user.edit', $user),
                ), array(
                    'label'   => 'Edit User',
                    'url'     => array('user/update', 'id' => $user->primaryKey),
                    'icon'    => 'edit',
                    'visible' => Yii::app()->user->checkAccess('user.edit', $user),
                ), array(
                    'label'       => 'Delete User',
                    'url'         => array('user/delete', 'id' => $user->primaryKey),
                    'icon'        => 'trash',
                    'linkOptions' => array('class' => 'delete'),
                    'visible'     => Yii::app()->user->checkAccess('admin') && Yii::app()->user->id != $user->id,
                ), array(
                    'label'       => 'Login as User',
                    'url'         => array('user/loginAs', 'id' => $user->primaryKey),
                    'icon'        => 'eye-open',
                    'visible'     => Yii::app()->user->checkAccess('admin') && Yii::app()->user->id != $user->id,
                ),
            ),
        )); ?>
        <?php Yii::app()->clientScript->registerScript('deleteButton', '
            $("a.delete").on("click", function(e) {
                e.preventDefault();
                if (confirm("Are you sure you want to delete this User?")) {
                    $.post($(this).attr("href"), function(){
                        window.location.href = ' . CJSON::encode($this->createUrl('user/list')) . ';
                    });
                }
            });
        ') ?>
    </div>
</div>

<div class="row">
    <div class="span6">
        <h3>(Commit) Aliases:</h3>
        <ul class="unstyled well well-small">
            <?php foreach ($user->aliases as $alias): ?>
            <li class="user-alias">
                <i class="icon-user"></i>
                <?php echo CHtml::encode($alias->alias) ?>
                <div class="pull-right">
                <?php if (Yii::app()->user->checkAccess('user.edit', $user)): ?>
                <?php echo CHtml::link('<i class="icon-trash"></i>', array('user/removeAlias', 'alias' => $alias->alias)) ?>
                <?php endif ?>
                </div>
            </li>
            <?php endforeach; ?>
            <?php Yii::app()->clientScript->registerScript('user-alias-hover', '
                $("li.user-alias").css("padding", "2px;").hover(function(){
                        $(this).css("border-bottom", "1px dashed #999");
                    }, function(){
                        $(this).css("border-bottom", "1px dashed transparent");
                    }
                );
                ') ?>
        </ul>
        <?php if (Yii::app()->user->checkAccess('user.edit', $user)): ?>
        <?php include '_alias_form.php' ?>
        <?php endif ?>
    </div>
    <div class="span6">
        <h3>Recent Reviews:</h3>
        <ul>
            <?php foreach ($comments as $comment): ?>
            <li>
                <?php include '_comment.php' ?>
            </li>
            <?php endforeach ?>
        </ul>
    </div>
</div>