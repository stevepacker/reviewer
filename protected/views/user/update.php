<?php
$this->breadcrumbs['Users'] = array('user/list');
$this->breadcrumbs[CHtml::encode($user->email)] = array('user/view', 'id' => $user->primaryKey);
$this->breadcrumbs[] = 'Edit';
?>

<h1>Edit User:</h1>

<?php include '_form.php'; ?>