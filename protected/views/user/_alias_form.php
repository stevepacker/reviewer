<?php $user_alias = new User_Alias; ?>
<?php /** @var BootActiveForm $form */
$form  = $this->beginWidget('bootstrap.widgets.BootActiveForm', array(
    'type' => 'inline',
    'action' => array('user/addAlias', 'id' => $user->primaryKey),
    'htmlOptions'=>array('class'=>'well'),
)); ?>

<?php echo CHtml::hiddenField('User_Alias[user_id]', $user->primaryKey) ?>
<?php echo $form->textFieldRow($user_alias, 'alias', array('class'=>'span3')); ?>

<?php $this->widget('bootstrap.widgets.BootButton', array(
    'buttonType'=>'submit',
    'type'=>'primary',
    'icon'=>'arrow-right white',
    'label'=>'Add Alias')); ?>

<?php $this->endWidget(); ?>