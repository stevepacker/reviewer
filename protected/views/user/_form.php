<div class="form">
<?php /** @var BootActiveForm $form */
$form  = $this->beginWidget('bootstrap.widgets.BootActiveForm', array(
    'type'        => 'horizontal',
    'htmlOptions' => array('class'=>'well'),
)); ?>

<?php echo $form->textFieldRow($user, 'display_name', array('prepend'=>'<i class="icon-user"></i>','class'=>'span3')); ?>
<?php echo $form->textFieldRow($user, 'email', array('prepend'=>'<i class="icon-envelope"></i>','class'=>'span3')); ?>
<?php $user->password = Arr::getDot($_POST, 'User.password') ?>
<?php echo $form->passwordFieldRow(
        $user,
        'password',
        array(
            'class'=>'span3',
            'hint' => $user->isNewRecord ? null : 'Leave password blank to not change it.',
        )); ?>
<?php echo CHtml::hiddenField('User[projects]', '') // so de-selecting all projects fires a save ?>
<?php echo $form->dropDownListRow(
        $user,
        'projects',
        CHtml::listData(Project::model()->findAll(), 'name', 'name'),
        array('multiple'=>true, 'class'=>'project-list')); ?>
<?php Yii::app()->clientScript->registerScript('project-list', '
        $.each(' . CJSON::encode(CHtml::listData($user->projects, 'name', 'name')) .', function(key, value){
            $(".project-list option[value=\"" + value + "\"]").attr("selected", "selected");
        });
') ?>
<?php if (Yii::app()->user->checkAccess('admin')): ?>
    <?php echo $form->checkBoxRow($user, 'status'); ?>
    <?php echo $form->checkBoxListRow(
            $user,
            'roles',
            User::getConstants('ROLE_'),
            array(
                'hint' => '<strong>Note:</strong> Checking "admin" authorizes all other roles.',
            )); ?>
<?php endif ?>

<div class="form-actions">
    <?php $this->widget('bootstrap.widgets.BootButton', array(
        'buttonType'=>'submit',
        'type'=>'primary',
        'icon'=>'ok white',
        'label'=>$user->isNewRecord ? 'Create' : 'Save')); ?>
    <?php $this->widget('bootstrap.widgets.BootButton', array(
        'buttonType'=>'reset',
        'icon'=>'remove',
        'label'=>'Reset')); ?>
</div>
<?php $this->endWidget(); ?>
</div>