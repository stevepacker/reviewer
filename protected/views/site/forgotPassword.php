<h1>Forgot your password?</h1>

<p>Enter your email address below and instructions to reset your password will be
emailed to you.</p>

<?php /** @var BootActiveForm $form */
$form = $this->beginWidget('bootstrap.widgets.BootActiveForm', array(
    'htmlOptions'=>array('class'=>'well'),
    'type' => 'inline',
)); ?>

<?php echo $form->textFieldRow($model, 'email', array('class'=>'span3')); ?>
<hr>
<?php $this->widget('bootstrap.widgets.BootButton', array('buttonType'=>'submit', 'icon'=>'ok', 'label'=>'Send Password Instructions')); ?>

<?php $this->endWidget(); ?>
<?php return; ?>