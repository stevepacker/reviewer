<div class="row">
    <div class="span6">
        <h1>Projects:</h1>
        <ul class="unstyled">
        <?php foreach ($projects as $project): ?>
            <li>
                <?php echo CHtml::link($project->name, array(
                    'project/view',
                    'id' => $project->name,
                )) ?>
            </li>
        <?php endforeach ?>
        <?php if (empty($projects)): ?>
            <li>
                <em>
                    <?php echo CHtml::link('Pick your projects', array(
                        'user/update',
                    )) ?>
                </em>
            </li>
        <?php endif ?>
        </ul>
    </div>
</div>

<hr>

<div class="row">
    <div class="span12">
    <h1>How does it work?</h1>
    <object data="<?php echo Yii::app()->baseUrl ?>/images/workflow.svg" type="image/svg+xml"></object>
    </div>
</div>