<p>
    Your code was recently reviewed.
    <?php echo CHtml::link('Go to review', $this->createAbsoluteUrl(
        'review/view', 
        array(
            'project' => $review->project,
            'commit'  => $review->commit,
        )
    )) ?>
</p>

<dl>
    <dt>Status:<dt>
    <dd><?php echo CHtml::encode($review->status) ?></dd>

    <dt>Commenter:</dt>
    <dd><?php echo CHtml::encode($comment->user->email) ?></dd>

    <dt>Comment:</dt>
    <dd><?php echo nl2br(CHtml::encode($comment->comment)) ?></dd>
</dl>