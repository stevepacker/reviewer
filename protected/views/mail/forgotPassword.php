<p>Greetings <?php echo CHtml::encode($username) ?>;</p>

<p>
We recently received a request to reset your password on the
<?php echo Yii::app()->name ?> website.  If you did not request this, you may
safely delete this email; your password will not be changed unless you visit
the link below and follow the steps there.
</p>

<p>
To proceed with resetting your password, please go to:
<?php echo CHtml::link($url, $url) ?>
</p>

<p>
Note: this link will automatically expire within 4 hours, or after it is utilized.
</p>


<p>
    Best Regards,<br>
    <?php echo Yii::app()->name ?> Support
</p>
