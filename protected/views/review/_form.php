<div class="form form-horizontal">
<?php /* @var BootActiveForm $form */
$form = $this->beginWidget('bootstrap.widgets.BootActiveForm', array(
    'htmlOptions'   => array('class'=>'well'),
    'type'          => 'horizontal',
    'id'            => 'comment_form',
)); ?>


    <?php if (Yii::app()->user->checkAccess('review.changeStatus', $review)): ?>
    <?php echo $form->radioButtonListInlineRow(
        $commentModel,
        'status',
        Review::getConstants('STATUS_')); ?>
    <?php endif ?>

    <?php echo $form->hiddenField($commentModel, 'commit') ?>
    <?php echo $form->hiddenField($commentModel, 'project') ?>
    <?php echo $form->textAreaRow(
        $commentModel,
        'comment',
        array(
            'class' => 'span8',
            'rows' => 5)); ?>


    <div class="form-actions">
        <?php $this->widget('bootstrap.widgets.BootButton', array(
            'buttonType'=>'submit',
            'type'=>'primary',
            'icon'=>'ok white',
            'label'=>'Save')); ?>
        or
        <?php echo CHtml::link('cancel', array('project/view', 'id' => $project->name)) ?>
    </div>


<?php $this->endWidget() ?>

<?php
if (Yii::app()->user->checkAccess('review.changeStatus', $review))
    Yii::app()->clientScript->registerScript('radio-focus-on-comment', '
(function(){
    var $target = $(".control-group:first");
    $target.on("focus, change, click", function(){
        $("#ReviewForm_comment").focus();
    });
})();
');
?>

</div>