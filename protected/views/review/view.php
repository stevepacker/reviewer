<?php
$this->breadcrumbs['Projects'] = array('project/list');
$this->breadcrumbs[CHtml::encode($project->name)] = array('project/view', 'id' => $project->name);
$this->breadcrumbs[] = 'Branch: ' . Arr::get(Yii::app()->session, "branch.{$project->name}");
$this->breadcrumbs[] = 'Reviewing commit: ' . Arr::get($_GET, 'commit');

$user = User::getUser($commit->getAuthor());

if (empty($review->status)) {
    $review->status       = Review::STATUS_STARTED;
    $commentModel->status = $review->status;
}

Yii::app()->clientScript
    ->registerScriptFile(Yii::app()->baseUrl . '/js/linkify.js')
    ->registerScript('linkify', '$(".well").each(function(){ $.linkify($(this)); });')
;
?>

<div class="notifications">
    <script type="text/javascript">
    if (!window.EventSource) {
        document.write('<div class="info">This browser does not support Server-Sent-Events (SSE).  Please switch to a modern browser (Firefox, Chrome, Safari).</div>')
    } else {
        var source = new EventSource(<?php echo CJSON::encode($this->createUrl(
            'review/viewers',
            array('commit' => $commit->hash))) ?>);

        source.addEventListener('message', function(e) {
            var data = $.parseJSON(e.data);
            if ($.isArray(data)) {
                var output = '';
                $.each(data, function(i, value) {
                    output += '<a href="' + value.profile + '">' + value.name + '</a>, ';
                });
                output = output.replace(/(, )$/, '');

                var alert = '<div class="alert alert-info">';
                if (0 === output.length) {
                    alert = '';
                }
                $('div.notifications').html(alert);
                $('div.notifications .alert-info').html("Current viewers: " + output + " ");
            }
        }, false);

        source.addEventListener('open', function(e) {
            // Connection was opened.
        }, false);

        source.addEventListener('error', function(e) {
            if (e.readyState == EventSource.CLOSED) {
                // Connection was closed.
            }
        }, false);
    }
    </script>
</div>

<h3 name="comments" style="margin: 20px 0 10px;">Author:</h3>

<div class="row">
    <div class="span2">
        <?php if ($user instanceof User): ?>
            <?php echo CHtml::link(CHtml::encode($user->display_name), array(
                'user/view', 'id' => $user->id,
            )) ?>
            <br>
            <?php echo CHtml::link(Html::gravitar($user->email, array(
                'default'     => 'mm',
                'htmlOptions' => array(
                    'class' => 'thumbnail',
                ),
            )), array('user/view', 'id' => $user->id)) ?>
        <?php else: ?>
            <?php echo $commit->getAuthor() ?>
        <?php endif ?>

    </div>
    <dl class="dl-horizontal span0">
        <dt>Commit Message:</dt>
        <dd>
            <?php echo nl2br(CHtml::encode($commit->subject)) ?>
        </dd>

        <dt>Timestamp:</dt>
        <dd><?php echo Yii::app()->format->datetime($commit->getTime()) ?></dd>

        <dt>Branches</dt>
        <dd><?php echo join('<br> ', $commit->getBranches()) ?></dd>

        <dt>Modified Files:</dt>
        <?php foreach ($commit->files as $filename): ?>
        <dd>
            <?php echo CHtml::ajaxLink(CHtml::encode($filename), array(
                    'review/file',
                    'filename' => $filename,
                    'project'  => $project->name,
                    'commit'   => $commit->hash,
                ), array( // ajax options
                    'success' => 'function(data) {
                        $(".diffViewer").remove();
                        $("<div class=\"diffViewer\">").appendTo("body");
                        $(".diffViewer").html(data);
                        diffRender();
                        $(".diffViewer").dialog({
                            title: ' . CJSON::encode($filename) . ',
                            width: "99%",
                            stack: false,
                            draggable: false
                        });
                        // fix overflow
                        $("table.diff td").css({ display:"block", width: $(window).width()/2, overflow: "auto"});
                        // scroll to first edit
                        var x = $("td.insert, td.empty, td.replace, td.delete").first();
                        $(window).scrollTop(x.offset().top - 100);
                    }'
                )) ?>
        </dd>
        <?php endforeach ?>
        <?php
        $clientScript = Yii::app()->clientScript;
        $clientScript
            ->registerCssFile(Yii::app()->baseUrl    . '/js/diffview.css')
            ->registerScriptFile(Yii::app()->baseUrl . '/js/diffview.js')
            ->registerScriptFile(Yii::app()->baseUrl . '/js/difflib.js')
            ->registerCssFile('//ajax.googleapis.com/ajax/libs/jqueryui/1.7/themes/start/jquery-ui.css')
            ->registerScriptFile('//ajax.googleapis.com/ajax/libs/jqueryui/1.7/jquery-ui.min.js')
        ;


        $clientScript->registerScript('jsdifflib', '
            var diffRender = function(){
                var basetxt       = difflib.stringAsLines($(".old").text());
                var newtxt        = difflib.stringAsLines($(".new").text());
                var diffoutputdiv = $(".diff");
                var sm            = new difflib.SequenceMatcher(basetxt, newtxt);
                diffoutputdiv.html(diffview.buildView({
                    baseTextLines: basetxt,
                    newTextLines:  newtxt,
                    opcodes:       sm.get_opcodes(),
                    baseTextName:  ' . CJSON::encode("(OLD)") . ',
                    newTextName:   ' . CJSON::encode("(NEW)") . ',
                    contextSize:   null,
                    viewType:      0 // 1=inline,0=side-by-side
                }));
            };
        ');
        ?>
    </dl>

    <div class="span1 pull-right">
        <?php $this->widget('bootstrap.widgets.BootLabel', array(
            'type'  => $review->convertStatusToFlash(),
            'label' => $review->status,
        )); ?>
    </div>
</div>

<h3 name="comments" style="margin: 20px 0 10px;">
    Reviews / Comments:
</h3>
<div class="row" style="margin-top: 10px;">
    <div class="span10">

    </div>
</div>

<?php foreach ($comments as $comment): ?>
<div class="row">
    <div class="span12">
        <?php include '_comment.php' ?>
    </div>
</div>
<?php endforeach; ?>

<div class="row">
    <div class="span12">
        <?php include '_form.php' ?>
    </div>
</div>
