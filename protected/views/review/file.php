<div class="row-fluid">
    <div class="diff"></div>
    <div class="old" style="display:none">
        <?php echo CHtml::encode($diffs['old']) ?>
    </div>
    <div class="new" style="display:none">
        <?php echo CHtml::encode($diffs['new']) ?>
    </div>
</div>