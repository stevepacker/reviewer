<div class="row-fluid" style="border-bottom: 1px solid #ccc; margin-bottom: 10px; padding-bottom: 10px;">
    <div class="span2">
        <?php echo CHtml::link(
                CHtml::encode($comment->user->display_name),
                array('user/view', 'id' => $comment->user->id))
        ?>
        <?php echo CHtml::link(Html::gravitar($comment->user->email, array(
            'default'     => 'mm',
            'htmlOptions' => array(
                'class' => 'thumbnail',
            ),
        )), array('user/view', 'id' => $comment->user->id)) ?>
    </div>
    <div class="span10">
        <div style="font-size: 11px;"><?php echo Yii::app()->format->datetime($comment->created) ?></div>
        <div class="well well-small">
            <?php echo nl2br(CHtml::encode($comment->comment)) ?>
        </div>
        <?php if ($comment->actions): ?>
        <?php foreach ($comment->actions as $field => $values): ?>
            <em>
                <i class="icon-flag"></i>
                <?php echo sprintf('%s changed from "%s" to "%s"',
                        ucfirst(CHtml::encode($field)),
                        CHtml::encode($values['old']),
                        CHtml::encode($values['new'])) ?>
            </em>
        <?php endforeach ?>
        <?php endif ?>
    </div>
</div>