<?php $review = Review::get($commit->hash, $model->name) ?>
<?php /* @var $review Review */
      /* @var $commit AGitCommit */
?>

<div class="row-fluid well-small well commit">
    <div class="span7">
        <?php $this->widget('bootstrap.widgets.BootButton', array(
            'label' => $review->status ? $review->status : 'unreviewed',
            'type'  => $review->convertStatusToFlash(), // '', 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'  => 'mini', // '', 'large', 'small' or 'mini'
            'url'   => $this->createUrl('review/view', array('project' => $model->name, 'commit' => $commit->hash)),
        )); ?>

        <?php $this->widget('bootstrap.widgets.BootBadge', array(
            'type'=>'', // '', 'success', 'warning', 'error', 'info' or 'inverse'
            'label'=>sprintf('%s %d files',
                    '<i class="icon-file icon-white"></i>',
                    count($commit->files)),
            'encodeLabel' => false,
        )); ?>

        <?php $this->widget('bootstrap.widgets.BootBadge', array(
            'type'=>'', // '', 'success', 'warning', 'error', 'info' or 'inverse'
            'label'=>sprintf('%s %d',
                    '<i class="icon-comment icon-white"></i>',
                    count($review->comments)),
            'encodeLabel' => false,
        )); ?>

        <small style="font-size: 11px;"><?php echo Yii::app()->format->datetime($commit->getTime()) ?></small>
        <br>
        <?php echo nl2br(CHtml::encode($commit->subject)) ?>
    </div>
    <dl class="span5 dl-horizontal" style="margin-top: 0;">
        <dt>Author:</dt>
        <dd>
            <?php if (($user = User::getUser($review->getCommit()->author))): ?>
                <?php echo CHtml::link(
                        CHtml::encode($review->getCommit()->author),
                        array(
                            'user/view',
                            'id' => $user->id,
                        )) ?>
            <?php else: ?>
                <span class="author">
                    <?php echo CHtml::encode($review->getCommit()->author) ?>
                </span>
                <?php echo CHtml::link('<i class="icon-user"></i> This is me', array(
                    'user/addAlias',
                    'alias' => $review->getCommit()->author,
                ), array(
                    'class' => 'btn btn-mini this-is-me',
                )) ?>
                <?php Yii::app()->clientScript->registerScript('this-is-me', '
                    $(".this-is-me").on("click", function(e){
                        e.preventDefault();
                        if (confirm("This will assign all commits with this author to your account. Do you wish to proceed?"))
                            $.post($(this).attr("href"), function(){
                                window.location.reload();
                            });
                    });
                ') ?>
            <?php endif ?>
        </dd>

        <dt>Commit:</dt>
        <dd>
            <small style="font-size: 11px;"><?php echo $commit->hash //substr($commit->hash, -7) ?></small></dd>

        <dt>Parent:</dt>
        <?php foreach ($commit->getParents() as $parent): ?>
        <dd>
            <?php echo CHtml::link(sprintf('%s', $parent->hash), array(
                'review/view', 'project' => $model->name, 'commit' => $parent->hash
            ), array('style' => 'font-size: 11px;')) ?>
        </dd>
        
        <?php endforeach ?>

        <?php if (!isset($bookmarkFound)): ?>
        <dt></dt>
        <dd>
            <?php if ($commit->hash === $model->bookmark): ?>
            <?php // $bookmarkFound = true; // prevents subsquent commits from being bookmarked ?>
            <i class="icon-bookmark"></i> 
            Production
            <?php else: ?>
            <?php echo CHtml::link(
                '<i class="icon-bookmark"></i>
                Bookmark as Production Commit', 
                array(
                    'project/bookmark', 
                    'id' => $model->name, 
                    'commit' => $commit->hash,
                ), array(
                    'class' => 'btn btn-mini',
                )) ?>
            <?php endif ?>
        </dd>
        <?php endif ?>
        
    </dl>
 </div>
