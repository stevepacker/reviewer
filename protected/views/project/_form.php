<?php
/** @var ActiveRecord $model */
/** @var BootActiveForm $form */
$form  = $this->beginWidget('bootstrap.widgets.BootActiveForm', array(
    'htmlOptions'=>array('class'=>'well'),
    'type' => 'horizontal'
)); ?>

<?php echo $form->textFieldRow($model, 'name', array('class'=>'span6')); ?>

<?php if ($model->getIsNewRecord() || Yii::app()->user->checkAccess('admin')): ?>
<?php echo $form->textFieldRow($model, 'uri',  array('class'=>'span6')); ?>
<?php endif ?>
<?php echo $form->radioButtonListRow($model, 'type', Project::getConstants('TYPE_')); ?>
<div class="form-actions">
    <?php $this->widget('bootstrap.widgets.BootButton', array(
        'buttonType'=>'submit',
        'type'=>'primary',
        'icon'=>'ok white',
        'label'=>'Save')); ?>
    <?php $this->widget('bootstrap.widgets.BootButton', array(
        'buttonType'=>'reset',
        'icon'=>'remove',
        'label'=>'Reset')); ?>
</div>
<?php $this->endWidget(); ?>