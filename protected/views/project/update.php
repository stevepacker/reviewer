<?php
$this->breadcrumbs['Projects'] = array('project/list');
$this->breadcrumbs[CHtml::encode($model->name)] = array('project/view', 'id' => $model->name);
$this->breadcrumbs[] = 'Edit';
?>

<h1>Edit Project: <?php echo CHtml::encode($model->name) ?></h1>
<?php require '_form.php' ?>