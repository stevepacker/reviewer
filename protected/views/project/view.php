<?php
$this->breadcrumbs['Projects'] = array('project/list');
$this->breadcrumbs[] = CHtml::encode($model->name);
?>

<div class="row">
    <h1 class="span7"><?php echo CHtml::encode($model->name) ?></h1>
    <div class="span5">
        <dl class="dl-horizontal">
            <dt>Branch:</dt>
            <dd>
                <?php
                $branches = array();
                foreach ($model->repo->branches as $a_branch) {
//                    if ($a_branch->name !== $branch) {
                    {
                        $branches[] = array(
                            'label' => $a_branch->name,
                            'url'   => array(
                                'project/setBranch',
                                'id'     => $model->name,
                                'branch' => $a_branch->name,
                            ),
                        );
                    }
                }
                $this->widget('bootstrap.widgets.BootButtonGroup', array(
                    'type'    => 'primary', // '', 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
                    'buttons' => array(
                        array(
                            'label'       => $branch,
                            'htmlOptions' => array('class' => 'btn-mini'),
                            'items'       => $branches,
                        ),
                    ),
                )); ?>
            </dd>

            <dt>Synced:</dt>
            <dd>
                <?php echo Yii::app()->format->datetime($model->updated) ?>

                <?php echo CHtml::link('<i class="icon-refresh"></i>', array('updateRepo', 'id' => $model->name), array('class' => 'btn btn-mini')) ?>
            </dd>
        </dl>
    </div>
</div>

<div class="commits">
    <?php foreach ($commits as $commit): /* @var $commit AGitCommit */ ?>
    <?php require '_commit.php' ?>
    <?php endforeach ?>
</div>


<?php $this->widget('bootstrap.widgets.BootButtonGroup', array(
    'type'          => '', // '', 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
    'size'          => 'large', // '', 'large', 'small' or 'mini'
    'htmlOptions'   => array(
        'id' => 'load-more-commits',
        'class' => 'dropup',
    ),
    'buttons'       => array(
        array(
            'icon'  => 'plus-sign',
            'label'         => 'Load More Commits',
            'url'           => array('project/view', 'id' => $model->name, 'limit' => 5),
        ),
        array('items' => array(
            array(
                'icon'  => 'plus',
                'label' => '5 more (default)',
                'url'   => array('project/view', 'id' => $model->name, 'limit' => 5),

            ),
            array(
                'icon'  => 'plus',
                'label' => '10 more',
                'url'   => array('project/view', 'id' => $model->name, 'limit' => 10),
            ),
            array(
                'icon'  => 'plus',
                'label' => '15 more',
                'url'   => array('project/view', 'id' => $model->name, 'limit' => 15),
            ),
            array(
                'icon'  => 'plus',
                'label' => '20 more',
                'url'   => array('project/view', 'id' => $model->name, 'limit' => 20),
            ),
            array(
                'icon'  => 'search',
                'label' => 'Since a particular commit',
                'url'   => array('project/view', 'id' => $model->name, 'since' => ''),
                'htmlOptions' => array('class' => 'since-commit'),
            ),
            array(
                'icon'  => 'bookmark',
                'label' => 'Since Production commit',
                'url'   => array('project/view', 'id' => $model->name, 'since' => $model->bookmark),
                'htmlOptions' => array('class' => 'since-commit'),
                'visible'     => !empty($model->bookmark),
            ),
        )),
    ),
)); ?>

<?php Yii::app()->clientScript->registerScript('load-more-commits', '

    (function(){
        var clicks = null;
        $("#load-more-commits a[href*=\"?since=\"]").on("click", function(e){
            e.preventDefault();

            var search = this.search + "&"; // query string
            var commit = search.replace(/.*?[&\?]since=(.*?)&.*/, "$1");
            if (0 == commit.length) {
                commit = $.trim(prompt("Commit Hash:"));
                if (commit.length) {
                    $(this).attr("href", $(this).attr("href") + commit + "&");
                }
            }
            
            if (commit.length) {
                $(".commits > div").remove();
            } else {
                // hit the cancel button
                e.stopImmediatePropagation();
            }
        });
        $("#load-more-commits > a:not(\".dropdown-toggle\"), #load-more-commits > ul a").on("click", function(e){
            e.preventDefault();

            $("#load-more-commits").hide();
            $(".commits").append(\'<div class="progress progress-striped progress-info active"><div class="bar" style="width:100%">Loading...</div>\');

            var offset = $(".commits > div").length;
            var url    = $(this).attr("href");
            if (-1 === this.search.search(/[&\?]since=[a-f0-9]+/))
                url += "&offset=" + offset;

            $.get(url, function(data){
                // wrap returned HTML in a div so its a single HTML element and we
                // can easily read into the first child element .commits
                var html = "<div>" + data + "</div>";
                $(html).each(function(i, el){
                    var commits = $(".commits > div.commit", el);
                    if (commits.length) {
                        commits.hide();
                        $(".commits").append(commits);
                        commits.fadeIn();
                        // move button to the bottom of commit list
                        $("#load-more-commits").appendTo(".commits").show();
                    }
                });
                $(".commits > .progress").remove();
            });
        });
    })();

') ?>