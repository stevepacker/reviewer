<?php $this->breadcrumbs[] = 'Projects' ?>

<h1>
    Projects:
    <?php echo CHtml::link('&plus; new', array('project/create'), array('class' => 'btn btn-mini btn-primary')) ?>
</h1>
<?php $this->widget('bootstrap.widgets.BootGridView', array(
    'dataProvider'=>$projects,
    'template'=>"{items}\n{pager}",
    'columns'=>array(
        array(
            'class' => 'CLinkColumn',
            'id'=>'name',
            'labelExpression' => 'CHtml::encode($data->name)',
            'urlExpression'   => 'array("project/view", "id" => $data->name)',
            'header'=>'Project',
        ),
        array('name'=>'uri', 'header'=>'URI', 'visible' => Yii::app()->user->checkAccess('admin')),
        array(
            'class'=>'bootstrap.widgets.BootButtonColumn',
            'htmlOptions'=>array('style'=>'width: 50px'),
        ),
    ),
)); ?>