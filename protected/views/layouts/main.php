<?php /* @var $this Controller */ ?>
<!DOCTYPE HTML>
<html lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <title><?php echo CHtml::encode(Yii::app()->params->itemAt('meta_title')) ?></title>
  <meta name="description"  content="<?php echo CHtml::encode(Arr::get(Yii::app()->params, 'meta_description')) ?>">
  <meta name="keywords"     content="<?php echo CHtml::encode(Arr::get(Yii::app()->params, 'meta_keywords')) ?>">
  <script src="//ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.min.js" type="text/javascript"></script>
  <?php
  /* @var $clientScript CClientScript */
  $clientScript = Yii::app()->getClientScript();
  $clientScript
      // launch all external links to new pages
      ->registerScript('external_links', '
          $("a[href^=\'http://\'], a[href^=\'https://\']").each(function(){
            if (null == $(this).attr("href").match(/^https?:\/\/[\w]+\./))
              $(this).attr("target", "_blank");
          });')
      ;

  ?>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>

<body class="bdc-<?php echo $this->id ?>">
<div class="container">
    <?php
    $project_items   = array();
    if (!Yii::app()->user->isGuest) {
        foreach (User::getUser()->getProjects() as $project) {
            $project_items[] = array(
                'label' => $project->name,
                'url'   => $this->createUrl('project/view', array('id' => $project->primaryKey)),
            );
        }
    }
    if (Yii::app()->user->checkAccess('project.manage')) {
        $project_items[] = array('---');
        $project_items[] = array(
            'label' => 'Manage Projects',
            'url'   => $this->createUrl('project/list'),
        );
    }
    $this->widget('bootstrap.widgets.BootNavbar', array(
        'fixed'     => false,
        'brand'     => 'Reviewer',
        'brandUrl'  => $this->createUrl('site/index'),
        'collapse'  => true, // requires bootstrap-responsive.css
        'items'     => array(
            array(
                'class'=>'bootstrap.widgets.BootMenu',
                'items'=>array(
                    array(
                        'label'   => 'Projects',
                        'items'   => $project_items,
                        'visible' => !Yii::app()->user->isGuest && !empty($project_items),
                    ),
                    array(
                        'label' => 'Users',
                        'url'   => $this->createUrl('user/list'),
                        'visible' => Yii::app()->user->checkAccess('admin'),
                    ),
                ),
            ),
//            '<form class="navbar-search pull-left" action=""><input type="text" class="search-query span2" placeholder="Search"></form>',
            array(
                'class'       => 'bootstrap.widgets.BootMenu',
                'htmlOptions' => array('class'=>'pull-right'),
                'items'       => array(
                    array(
                        'label'   =>'My Account',
                        'url'     => $this->createUrl('user/view'),
                        'visible' => !Yii::app()->user->isGuest,
                    ),
                    array(
                        'label'   =>'Logout',
                        'url'     => $this->createUrl('site/logout'),
                        'visible' => !Yii::app()->user->isGuest,
                    ),
                    array(
                        'label'   =>'Login',
                        'url'     => $this->createUrl('site/login'),
                        'visible' => Yii::app()->user->isGuest,
                    ),
                ),
            ),
        ),
    )); ?>
    <?php $this->widget('bootstrap.widgets.BootBreadcrumbs', array(
        'links'    => $this->breadcrumbs,
        'homeLink' => false,
    )); ?>
    <?php $this->widget('bootstrap.widgets.BootAlert'); ?>
    <?php echo $content; ?>
</div>
</body>
</html>