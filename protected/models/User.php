<?php
class User extends ActiveRecord
{
    const STATUS_ENABLED  = 1;
    const STATUS_DISABLED = 0;

    const ROLE_ADMIN = 'admin';

    public $id;
    public $display_name;
    public $email;
    public $password;
    public $salt;
    public $status;
    public $roles = array();
    public $created;
    public $updated;
    public $last_login;
    public $last_ip;

    /**
     * Cache of Users fetched by User::getUser($pk);
     *
     * @var array [$pk: $user]
     */
    private static $_users;

    public function rules()
    {
        return array(
            array('email, display_name', 'required'),
            array('email', 'email'),
            array('email', 'unique'),
            array('password', 'required', 'on' => 'insert, updatePassword'),
            // Password must contain at least one letter, at least one number,
            // and be longer than six charaters
            array('password', 'match', 'pattern' => '/^(?=.*[0-9]+.*)(?=.*[a-zA-Z]+.*)[0-9a-zA-Z]{6,}$/',
                'on' => 'insert',
                'message' => 'Password must contain at least one letter, at '
                            .'least one number, and be longer than six charaters'),
            array('password', 'filter',   'filter' => array($this, 'hashPassword'),
                'on' => 'insert, updatePassword'),
            array('salt, created, updated, last_login, last_ip', 'unsafe'),
            array('status', 'in',      'range' => self::getConstants('STATUS_')),
            array('status', 'default', 'value' => self::STATUS_ENABLED),
            array('display_name', 'length', 'max' => 60),
            array('display_name', 'unique'),
            array('roles', 'safe'),
        );
    }

    public function behaviors()
    {
        return array(
            'CTimestampBehavior' => array(
                'class'             => 'zii.behaviors.CTimestampBehavior',
                'createAttribute'   => 'created',
                'updateAttribute'   => 'updated',
                'setUpdateOnCreate' => false,
            ),
            'JsonFieldBehavior' => array(
                'class' => 'JsonFieldBehavior',
                'fields' => array(
                    'roles',
                ),
            ),
        );
    }

    public function relations()
    {
        return array(
            'aliases'   => array(self::HAS_MANY, 'User_Alias',   'user_id'),
            '_projects' => array(self::HAS_MANY, 'Project_User', 'user_id'),
            'settings'  => array(self::HAS_MANY, 'User_Setting', 'user_id'),
            'comments'  => array(self::HAS_MANY, 'Comment',      'user_id'),
        );
    }

    public function attributeLabels()
    {
        return array(
            'status'  => 'Enabled',
            'last_ip' => 'Last IP Address',
            'roles'   => 'Permissions',
        );
    }

    public function afterSave()
    {
        if ($this->isNewRecord) {
            foreach (explode(',', User_Setting::DEFAULT_ON_SETTINGS) as $setting) {
                $model          = new User_Setting;
                $model->user_id = $this->id;
                $model->setting = trim($setting);
                $model->value   = true;
                $model->save();
            }
        }
    }

    public function delete() {
        foreach ($this->projects as $project) {
            $project->delete();
        }
        foreach ($this->settings as $setting) {
            $setting->delete();
        }
        foreach ($this->aliases as $alias) {
            $alias->delete();
        }
        foreach ($this->comments as $comment) {
            $comment->delete();
        }
        parent::delete();
    }

    /**
     * Hashes a plain-text version of password for storage or authentication.
     * This method will also generate a user-salt if one does not already exist.
     *
     * @param string $password plain-text version of password
     */
    public function hashPassword($password)
    {
        // ensure user's salt exists and is not empty
        if (empty($this->salt))
        {
            $this->salt = substr(md5(microtime(TRUE)), 0, 10);
            // don't auto-save if user is a new record (there will obviously be
            // a $this->save() coming right after this is done).
            if (!$this->isNewRecord)
                $this->update(array('salt'));
        }
        $siteSalt = Setting::get('system.salt');
        return sha1($siteSalt . $password . $this->salt);
    }

    /**
     * Shortcut to find a User by an alias
     *
     * @param string $alias
     * @return User|null
     */
    public function findByAlias($alias)
    {
        $this->with('aliases');
        foreach (func_get_args() as $arg) {
            $this->getDbCriteria()
                 ->addSearchCondition('alias', $arg, true, 'OR');
        }
        return $this->find();
    }

    /**
     * Helper function to fetch user by various means.
     *
     * @param string|int $pk Can be a user_id, email, or alias.  Default:
     *                       Yii::app()->user->getUser() will be used.
     * @return User or null if no User is found
     */
    public static function getUser($pk = null, $createIfEmpty = false)
    {
        // cached
        if (!empty(self::$_users[$pk]))
            return self::$_users[$pk];

        // logged-in user
        if (null === $pk && Yii::app()->hasComponent('user')) {

            return ! Yii::app()->user->isGuest
                   ? Yii::app()->user->getUser()
                   : null;
        }

        // user_id
        if (is_numeric($pk)) {
            return self::model()->findByPk($pk);
        }

        // email
        if (is_scalar($pk) && false !== strpos($pk, '@')) {
            $user = self::model()->findByAttributes(array(
                'email' => $pk,
            ));
            if (!empty($user)) {
                self::$_users[$pk] = $user;
                return $user;
            }
        }

        // alias
        $user = self::model()->findByAlias($pk);

        // create if empty toggle
        if ($createIfEmpty) {
            $user = new User;
        }

        self::$_users[$pk] = $user;

        return $user;
    }

    /**
     * RBAC for editing a User
     *
     * @param User $user
     * @return boolean
     */
    public function canEdit(User $user = null)
    {
        if (null === $user && Yii::app()->hasComponent('user')) {
            $user = self::getUser();
        }

        return ($this->id == $user->id);
    }
    
    public function canEditAlias(User $user = null)
    {
        return $this->canEdit($user);
    }

    /**
     * Override to allow projects to be ran through self::setProjects() instead
     *
     * @param array $values
     * @param boolean $safeOnly Default: true
     * @return null
     */
    public function setAttributes($values, $safeOnly = true) {
        if (array_key_exists('projects', $values)) {
            $this->setProjects($values['projects']);
            unset($values['projects']);
        }
        return parent::setAttributes($values, $safeOnly);
    }

    /**
     * Helper to convert an array of project mappings to an array of Pproject models
     *
     * @return array of Project models
     */
    public function getProjects()
    {
        $projects = array();
        foreach ($this->_projects as $project_map) {
            $projects[$project_map->project->name] = $project_map->project;
        }
        return $projects;
    }

    public function setProjects($value)
    {
        Project_User::model()->deleteAllByAttributes(array(
            'user_id' => $this->id,
        ));
        if (is_array($value)) {
            foreach ($value as $project_name) {
                $map = new Project_User;
                $map->user_id = $this->id;
                $map->project_name = $project_name;
                $map->save();
            }
        }
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria=new CDbCriteria;

//    public $id;
//    public $display_name;
//    public $email;
//    public $password;
//    public $salt;
//    public $status;
//    public $roles = array();
//    public $created;
//    public $updated;
//    public $last_login;
//    public $last_ip;

        foreach (array(
            'display_name',
            'email',
            'status',
            'roles',
        ) as $key) {
            $criteria->compare($key, $this->{$key}, true);
        }

        return new CActiveDataProvider(get_class($this), array(
            'criteria'   => $criteria,
            'pagination' => array(
                'pageSize' => 5
            ),
        ));
    }

    /**
     * Retrieves the value of a setting, or $default if not found
     * @param string $name
     * @param mixed $default
     * @return mixed
     */
    public function getSetting($name, $default = null)
    {
        foreach ($this->settings as $setting) {
            if ($name === $setting->setting) {
                return $setting->value;
            }
        }
        return $default;
    }
}