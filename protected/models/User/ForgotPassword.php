<?php

/**
 * This is the model class for table "users".
 *
 * The followings are the available columns in table 'users':
 * @property integer $user_id
 * @property string $token
 * @property string $creation_date
 * @property string $modified_date
 * @property string $creation_ip
 * @property string $modified_ip
 *
 * The followings are the available model relations:
 * @property Accounts[] $accounts
 * @property Campaigns[] $campaigns
 */
class User_ForgotPassword extends ActiveRecord
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'user_forgot_passwords';
    }

    /**
     * Auto-sets date fields on insert/update
     * @return array
     */
    public function behaviors()
    {
        return array(
            'CTimestampBehavior' => array(
                'class' => 'zii.behaviors.CTimestampBehavior',
                'createAttribute'   => 'created',
                'updateAttribute'   => 'modified',
                'setUpdateOnCreate' => false,
            ),
        );
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('user_id, token', 'required'),
            array('token', 'length', 'is' => 8),
            array('created_ip',  'default', 'value' => Yii::app()->request->getUserHostAddress()),
            array('modified_ip', 'default', 'value' => Yii::app()->request->getUserHostAddress(), 'on' => 'update'),

            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('token, creation_date, creation_ip, modified_date, modified_ip', 'safe', 'on'=>'search'),
        );
    }

    public function relations()
    {
        return array(
            'user' => array(self::BELONGS_TO, 'User', 'user_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'user_id'       => 'User ID',
            'token'         => 'Token',
            'created'       => 'Created on',
            'created_ip'    => 'Created by',
            'modified'      => 'Modified on',
            'modified_ip'   => 'Modified by',
        );
    }

    public function defaultScope() {
        /* @todo why isn't this working? */
        return array(
            'scopes' => 'unexpired',
        );
    }

    public function scopes()
    {
        return array(
            'unexpired' => array(
                'condition' => 'modified IS NULL AND created > DATE_SUB(NOW(), INTERVAL 4 HOUR)'
            ),
        );
    }

    /**
     * Custom scope to filter by user_id
     *
     * @param string|User $user
     * @return User_ForgotPassword
     */
    public function userScope($user)
    {
        if ($user instanceof User)
            $user = $user->id;

        $this->getDbCriteria()->compare('user_id', $user);

        return $this;
    }

    /**
     * Custom scope to filter by token
     *
     * @param string $token
     * @return User_ForgotPassword
     */
    public function tokenScope($token)
    {
        $this->getDbCriteria()->compare('token', $token);

        return $this;
    }
}