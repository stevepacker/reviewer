<?php
class User_Alias extends ActiveRecord
{
    public function rules()
    {
        return array(
            array('user_id, alias', 'required'),
            array('alias', 'length', 'max' => 255),
        );
    }

    public function relations()
    {
        return array(
            'user' => array(self::BELONGS_TO, 'User', 'user_id'),
        );
    }

    public function attributeLabels()
    {
        return array(
            'alias' => 'Alternative email or username'
        );
    }
}