<?php

class User_Setting extends ActiveRecord
{
    /**
     * A comma-separated list of settings that should be automatically checked
     * when a user is created.  See User::afterSave()
     */
    const  DEFAULT_ON_SETTINGS = 'email_myCodeReviewed, email_myReviewCommented';

    public $user_id;
    public $setting;
    public $value;

    public function relations() {
        return array(
            'user' => array(self::BELONGS_TO, 'User', 'user_id'),
        );
    }

    public function behaviors() {
        return array(
            'JsonFieldBehavior' => array(
                'class'  => 'JsonFieldBehavior',
                'fields' => array('value')
            )
        );
    }
}