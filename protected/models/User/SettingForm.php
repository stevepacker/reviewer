<?php

class User_SettingForm extends CFormModel
{
    public $email_myCodeReviewed;
    public $email_myReviewCommented;

    public function rules()
    {
        return array(
            array('email_myCodeReviewed, email_myReviewCommented', 'boolean'),
            array('email_myCodeReviewed, email_myReviewCommented', 'forceBoolean'),
        );
    }

    public function attributeLabels() {
        return array(
            'email_myCodeReviewed'    => 'Email me when my code has been reviewed.',
            'email_myReviewCommented' => 'Email me when additional comments occur on a review I participated in.',
        );
    }

    /**
     * Instantiates a new model and populates it by $user
     *
     * @param User $user
     * @return User_SettingForm
     */
    public static function loadFromUser(User $user)
    {
        $form = new self;
        foreach (User_Setting::model()->findAllByAttributes(array('user_id' => $user->id))
                as $setting) {
            $form->{$setting->setting} = $setting->value;
        }
        return $form;
    }

    public function forceBoolean($name, $params = array())
    {
        $this->{$name} = (boolean) $this->{$name};
    }
}