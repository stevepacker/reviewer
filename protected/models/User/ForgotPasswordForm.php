<?php

class User_ForgotPasswordForm extends CFormModel
{
    public $email;

    public function rules()
    {
        return array(
            array('email', 'required'),
            array('email', 'length', 'max' => 255),
        );
    }
}