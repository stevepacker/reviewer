<?php

class Review extends ActiveRecord
{
    const STATUS_STARTED    = 'started';
    const STATUS_ACTIONABLE = 'actionable';
    const STATUS_APPROVED   = 'approved';

    public $project;
    public $commit;
    public $created;
    public $updated;
    public $status;

    private $_project;

    public function rules()
    {
        return array(
            array('project, commit',  'required'),
            array('created, updated', 'date', 'format' => self::DATETIME_FORMAT),
            array('created, updated', 'unsafe'),
            array('status',  'in', 'range' => self::getConstants('STATUS_')),
            array('status', 'default', 'value' => self::STATUS_STARTED),
        );
    }

    public function behaviors()
    {
        return array(
            'CTimestampBehavior' => array(
                'class'             => 'zii.behaviors.CTimestampBehavior',
                'createAttribute'   => 'created',
                'updateAttribute'   => 'updated',
                'setUpdateOnCreate' => true,
            ),
        );
    }

    public function relations()
    {
        return array(
//            'comments' => array(self::HAS_MANY, 'Comment', )
        );
    }

    public function getComments()
    {
        return Comment::model()->subject($this)->findAll();
    }

    public function getProject()
    {
        if (empty($this->_project)) {
            $this->_project = Project::model()->findByPk($this->project);
        }

        return $this->_project;
    }

    public static function get($hash, $project)
    {
        $model = self::model()->findByPk(array(
            'commit'  => $hash,
            'project' => $project));
        if (empty($model)) {
            $model = new self;
            $model->commit  = $hash;
            $model->project = $project;
        }
        return $model;
    }

    public function getCommit()
    {
        return $this->getProject()->repo->getCommit($this->commit);
    }

    public function convertStatusToFlash()
    {
        switch ($this->status) {
            case self::STATUS_APPROVED:
                return 'success';
            case self::STATUS_STARTED:
                return 'primary';
            case self::STATUS_ACTIONABLE:
                return 'warning';
            default:
                return 'danger';
        }
    }

    public function canChangeStatus(User $user = null)
    {
        if (null === $user) {
            $user = User::getUser();
        }

        if (($commit = $this->getCommit())) {
            $author  = User::getUser($commit->author);
            if (null === $author)
                return true;
            return ($author->id != $user->id);
        }
        return false;
    }

    public function getFileDiff($filename)
    {
        $project = $this->getProject();
        if (!$project)
            throw new CException('Unable to retrieve project.');

        $commit = $project->repo->getCommit($this->commit);
        if (!$commit)
            throw new CException('Unable to retrieve commit.');

        if (!in_array($filename, $commit->files))
            throw new CException('File not found in committed files.');


        $old_file_contents = '';
        $parents = $commit->parents;
        if (!empty($parents))
        {
            $parent_commit = array_shift($parents);
            try {
                $old_file_contents = $parent_commit->getFileContents($filename);
            } catch (AGitException $e) {
                $old_file_contents = ''; // new file in commit
            }
        }

        try {
            $new_file_contents = $commit->getFileContents($filename);
        } catch (AGitException $e) {
            $new_file_contents = ''; // deleted file in new commit
        }

        return array(
            'old' => $old_file_contents,
            'new' => $new_file_contents,
        );
    }
}