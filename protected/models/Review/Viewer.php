<?php

class Review_Viewer extends CModel
{
    /**
     * String used to uniquely key cache value.  Intended to be ran through sprintf()
     * to create a unique key.
     */
    const CACHE_KEY = 'reviewViewer-%s';

    /**
     * Seconds before a user is considered stale.  Since EventSource will re-open a
     * connection ~3 seconds after disconnect, this should be greater than 3 seconds.
     */
    const USER_STALE_SECS = 10;

    /**
     * @var string Commit hash or number
     */
    public $commit;

    /**
     * @var CAttributeCollection list of viewers with the user_id as the key, and
     * the last seen timestamp as the value
     */
    public $data;

    /**
     * @var string The cache key to use as formulated by sprintf() in __construct()
     */
    private $_cache_key;

    /**
     * Required by CModel, this is a list of attributes.
     * @return array
     */
    public function attributeNames() {
        return array('commit', 'data');
    }

    /**
     * Helper function to return an instantiated instance of this model
     *
     * @param string $key
     * @return \self
     */
    public static function get($key)
    {
        $obj = new self($key);
        $obj->refresh();
        return $obj;
    }

    /**
     * Instantiate variables
     *
     * @param string  $commit
     * @param boolean $createIfEmpty
     */
    public function __construct($commit, $createIfEmpty = true)
    {
        $this->commit     = $commit;
        $this->data       = new CAttributeCollection;
        $this->_cache_key = sprintf(self::CACHE_KEY, $commit);
    }

    /**
     * Pull data out of cache
     */
    public function refresh()
    {
        $data = Yii::app()->cache->get($this->_cache_key);
        if (!empty($data)) {
            $this->data->copyFrom($data);
        }

        $this->pruneStaleViewers();
    }

    /**
     * Remove stale visitors from collection
     */
    public function pruneStaleViewers()
    {
        foreach ($this->data as $user_id => $timestamp) {
            if (self::USER_STALE_SECS < (time() - $timestamp)) {
                unset($this->data[$user_id]);
            }
        }
    }

    /**
     * Add a viewer into the data
     *
     * @param int       $user_id
     * @param boolean   $autoSave default: true
     */
    public function addViewer($user_id, $autoSave = true)
    {
        $this->data->add($user_id, time());
        if ($autoSave) {
            $this->save();
        }
    }

    /**
     * Retrieves the users in the collection.  Note this selects specific attributes
     * (id, display_name, roles) to limit data leakage.
     *
     * @return array Users[]
     */
    public function getUsers()
    {
        $data  = $this->data;
        $model = User::model();
        $model->getDbCriteria()->select = array('id', 'display_name', 'roles');
        return $model->findAllByPk($data->keys);
    }

    /**
     * Saves data back to cache.
     *
     * @return boolean whether the save action was successful
     */
    public function save()
    {
        return Yii::app()->cache->set(
            $this->_cache_key,
            $this->data,
            self::USER_STALE_SECS);
    }
}
