<?php

class Comment extends ActiveRecord
{
    public $subject_class;
    public $subject_pk;
    public $comment;
    public $user_id;
    public $created;
    public $updated;
    public $actions;

    public function rules()
    {
        return array(
            array('subject_class, subject_pk', 'required'),
            array('subject_pk', 'length', 'max' => 255),
            array('comment', 'length', 'min' => 1, 'max' => self::TEXT_MAXLENGTH),
            array('created, updated', 'date', 'format' => self::DATETIME_FORMAT),
            array('created, updated, user_id', 'unsafe'),
            array('user_id', 'default', 'value' => Yii::app()->user->id),
        );
    }

    public function behaviors()
    {
        return array(
            'CTimestampBehavior' => array(
                'class'             => 'zii.behaviors.CTimestampBehavior',
                'createAttribute'   => 'created',
                'updateAttribute'   => 'updated',
                'setUpdateOnCreate' => true,
            ),
            'JsonFieldBehavior' => array(
                'class' => 'JsonFieldBehavior',
                'fields' => array(
                    'subject_pk',
                    'actions',
                ),
            ),
        );
    }

    public function relations()
    {
        return array(
            'user' => array(self::BELONGS_TO, 'User', 'user_id'),
        );
    }

    public function scopes()
    {
        return array(
            'oldestFirst' => array(
                'order' => 'created DESC',
            ),
            'newestFirst' => array(
                'order' => 'created ASC',
            ),
        );
    }

    /**
     * Custom scope for isolating comments from a particular user
     *
     * @param int|User $user Either a user_id, or a User object
     * @return Comment
     */
    public function ofUser($user)
    {
        if ($user instanceof User)
            $user = $user->primaryKey;

        $this->getDbCriteria()
             ->compare('user_id', $user);
        return $this;
    }

    /**
     * Custom scope for isolating comments belonging to a certain object
     *
     * @param ActiveRecord $subject
     * @return Comment
     */
    public function subject(ActiveRecord $subject)
    {
//        if (is_object($subject)) {
            $this->getDbCriteria()
                 ->addColumnCondition(array(
                     'subject_class' => get_class($subject),
                     'subject_pk'    => CJSON::encode($subject->getPrimaryKey()),
                 ));
//        }

        return $this;
    }

    public function setSubject(ActiveRecord $subject)
    {
        $this->subject_class = get_class($subject);
        $this->subject_pk    = $subject->getPrimaryKey();
    }

    /**
     * Retrieves the subject model from a comment.
     *
     * @param boolean $createIfEmpty
     * @return ActiveRecord
     */
    public function getSubject($createIfEmpty = false)
    {
        $class = $this->subject_class;
        $subject = $class::model()->findByPk($this->subject_pk);
        if (empty($subject) && $createIfEmpty) {
            /* @var $subject ActiveRecord */
            $subject  = new $class;
            $pk_field = $subject->primaryKey();
            $subject->$pk_field = $this->subject_pk;
        }
        return $subject;
    }
}