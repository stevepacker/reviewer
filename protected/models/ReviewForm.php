<?php

class ReviewForm extends CFormModel
{
    public $commit;
    public $project;
    public $comment;
    public $status = Review::STATUS_STARTED;

    public function rules()
    {
        return array(
//            array('status', 'required'),
            array('comment', 'length', 'max' => ActiveRecord::TEXT_MAXLENGTH),
            array('status',  'in', 'range' => Review::getConstants('STATUS_')),
            array('commit',  'match', 'pattern' => '/^[a-f0-9]{1,40}/i'),
            array('project', 'length', 'max' => 255),
        );
    }

    public function save()
    {
        if (!$this->validate()) {
            return false;
        } else {
            $review  = $this->getReview();
            $review_prev_status = $review->status;
            if ($review->status != $this->status) {
                $review_prev_status = $review->status;
                $review->status     = $this->status;
                if (!$review->save()) {
                    foreach ($review->errors as $key => $error_messages) {
                        foreach ($error_messages as $error_message) {
                            $this->addError('status', $error_message);
                        }
                    }
                }
            }

            $comment = new Comment;
            $comment->subject  = $review;
            $comment->comment  = $this->comment;
            $comment->actions  = array(
                'status' => array(
                    'old' => empty($review_prev_status) ? 'unreviewed' : $review_prev_status,
                    'new' => $review->status,
                )
            );
            if ($comment->actions['status']['old'] === $comment->actions['status']['new'])
                unset($comment->actions['status']);

            if (!$comment->save()) {
                foreach ($comment->errors as $key => $error_messages) {
                    foreach ($error_messages as $error_message) {
                        $this->addError('comment', $error_message);
                    }
                }
            }

            // send email to code author, if enabled
            $commit_author = User::getUser($review->getCommit()->author);
            $subject       = Yii::app()->name . sprintf(' [%s]', substr($review->commit, 0, 7));
            $author_notified = false;
            if ($commit_author) {
                if ($comment->user_id != $commit_author->id
                    && $commit_author->getSetting('email_myCodeReviewed', true)) {
                    $mail = new YiiMailMessage;
                    $mail->view    = 'myCodeReviewed';
                    $mail->from    = Yii::app()->params['adminEmail'];
                    $mail->subject = $subject;
                    $mail->addTo($commit_author->email);
                    $mail->setBody(array(
                        'comment' => $comment,
                        'review'  => $review,
                    ), 'text/html');
                    Yii::app()->mail->send($mail);
                    $author_notified = true;
                }
            }

            if (Yii::app()->hasComponent('user')) {
                $user = Yii::app()->user->getUser();
                // get a list of unique recipients
                $recipients = array();
                foreach ($review->comments as $review_comment) {
                    if ($review_comment->user_id != $user->id
                        // Ensure commit author is not notified twice
                        && (! $commit_author || ! $author_notified || $review_comment->user_id != $commit_author->id)
                        && $review_comment->user->getSetting('email_myReviewCommented', true)) {
                        $recipients[$review_comment->user_id] = $review_comment->user->email;
                    }
                }
                foreach ($recipients as $recipient_email) {
                    $mail = new YiiMailMessage;
                    $mail->view    = 'myReviewCommented';
                    $mail->from    = Yii::app()->params['adminEmail'];
                    $mail->subject = $subject;
                    $mail->addTo($recipient_email);
                    $mail->setBody(array(
                        'comment' => $comment,
                        'review'  => $review,
                    ), 'text/html');
                    Yii::app()->mail->send($mail);
                }
            }

            return !$comment->hasErrors() && !$review->hasErrors();
        }
    }

    public function getReview()
    {
        $review = Review::model()->findByAttributes(array(
            'project' => $this->project,
            'commit'  => $this->commit,
        ));
        if (empty($review)) {
            $review = new Review;
            $review->project = $this->project;
            $review->commit  = $this->commit;
        }
        return $review;
    }
}
