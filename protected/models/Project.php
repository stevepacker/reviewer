<?php

class Project extends ActiveRecord
{
    const TYPE_GIT = 'git';
//    const TYPE_SVN = 'svn';
    
    const SETTING_KEY_BOOKMARK = 'project.%s.bookmark';

    public $name;
    public $type;
    public $uri;
    public $created;
    public $updated;

    private $_repo;

    public function rules()
    {
        return array(
            array('name, uri', 'required'),
            array('name', 'unique'),
            array('name', 'length', 'min' => 1, 'max' => 255),
            array('type', 'in', 'range' => self::getConstants('TYPE_')),
            array('created, updated', 'date', 'format' => ActiveRecord::DATETIME_FORMAT),
            array('created,updated', 'unsafe'),
        );
    }

    public function attributeLabels() {
        return array(
            'name' => 'Project Name',
            'uri'  => 'Git Repo URI'
        );
    }

    public function behaviors()
    {
        return array(
            'CTimestampBehavior' => array(
                'class'             => 'zii.behaviors.CTimestampBehavior',
                'createAttribute'   => 'created',
                'updateAttribute'   => 'updated',
                'setUpdateOnCreate' => true,
            ),
        );
    }

    public function relations()
    {
        return array(
            'users' => array(self::HAS_MANY, 'Project_User', 'project_name')
        );
    }

    /**
     * Custom scope to find a project based on a directory name
     *
     * @param string $directory
     */
    public function directoryScope($directory)
    {
        $this->getDbCriteria()
             ->addSearchCondition('name', str_replace('-', '%', $directory), false);

        return $this;
    }

    public function getId()
    {
        Yii::log('Wrong attribute name', CLogger::LEVEL_WARNING);
        return $this->name;
    }

    public function afterSave()
    {
        if ($this->getIsNewRecord())
            $this->getRepo()->cloneRemote($this->uri);

        return parent::afterSave();
    }

    public function getRepo()
    {
        if (null === $this->_repo) {
            $this->_repo = new Repository($this);
        }

        return $this->_repo;
    }

    public function beforeDelete() {
        foreach ($this->users as $user_mapping) {
            $user_mapping->delete();
        }

        return parent::beforeDelete();
    }
    
    public function afterDelete()
    {
        $this->repo->delete();
        return parent::afterDelete();
    }
    
    public function getBookmark()
    {
        return Setting::get(sprintf(self::SETTING_KEY_BOOKMARK, $this->name));
    }
    
    public function setBookmark($value)
    {
        return Setting::set(sprintf(self::SETTING_KEY_BOOKMARK, $this->name), $value);
    }
}