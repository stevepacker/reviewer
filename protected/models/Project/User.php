<?php
class Project_User extends ActiveRecord
{
    public function rules()
    {
        return array(
            array('user_id, project_name', 'required'),
            array('project_name', 'length', 'max' => 255),
            array('role', 'length', 'max' => 60),
        );
    }

    public function relations()
    {
        return array(
            'user'      => array(self::BELONGS_TO, 'User', 'user_id'),
            'project'   => array(self::BELONGS_TO, 'Project', 'project_name'),
        );
    }
}