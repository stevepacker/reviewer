<?php

class Repository extends CComponent
{
    private $_project;
    private $_driver;

    public function __construct(Project $project)
    {
        $this->_project = $project;

        $driver = 'Repository_Driver_' . ucfirst(Utils::camelToUnderscore($project->type));
        if (!class_exists($driver))
            throw new CException('Unable to find driver for Repository type: ' . $project->type);

        $this->_driver  = new $driver($project);
    }

    public function getProject()
    {
        return $this->_project;
    }

    public function __get($name) {
        return $this->_driver->$name;
    }

    public function __call($name, $parameters) {
        return call_user_func_array(
                array($this->_driver, $name),
                $parameters);
    }

    public function delete()
    {
        $this->_rmdir_recursive($this->repo->repoPath);
    }

    private function _rmdir_recursive($dir)
    {
        if (false === strpos($dir, Yii::app()->basePath . '/runtime/repos/'))
            throw new CException('Unable to delete directory: ' . var_export($dir, true));

        if (!file_exists($dir))
            return true;
        if (!is_dir($dir) || is_link($dir))
            return unlink($dir);

        foreach (scandir($dir) as $item) {
            if ($item == '.' || $item == '..')
                continue;
            if (!$this->_rmdir_recursive($dir . DIRECTORY_SEPARATOR . $item)) {
                chmod($dir . DIRECTORY_SEPARATOR . $item, 0777);
                if (!$this->_rmdir_recursive($dir . DIRECTORY_SEPARATOR . $item))
                    return false;
            }
        }
        return rmdir($dir);
    }
}