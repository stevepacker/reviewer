<?php
class Setting extends ActiveRecord
{
    private static $_s;

    /**
     * Preload setting variables into memory
     *
     * @return null
     */
    private static function _preload()
    {
        if (null === self::$_s)
            foreach (self::model()->findAll() as $row)
                self::$_s[$row->key] = $row->value;
    }

    /**
     * Shortcut static function to lookup a value
     *
     * @param string $key
     * @param mixed $default
     * @return mixed
     */
    public static function get($key, $default = null)
    {
        self::_preload();
        return isset(self::$_s[$key])
            ? self::$_s[$key]
            : $default;
    }
    
    /**
     * Shortcut static function to set a value to a key
     * 
     * @param string $key
     * @param mixed $value
     * @return boolean if the save was successful
     */
    public static function set($key, $value)
    {
        $row = self::model()->findByPk($key);
        if (!$row) {
            $row = new self;
            $row->key = $key;
        }
        $row->value = $value;
        return $row->save();
    }

    public function afterFind()
    {
        $this->value = CJSON::decode($this->value);
        return parent::afterFind();
    }

    public function beforeSave()
    {
        $this->value = CJSON::encode($this->value);
        return parent::beforeSave();
    }

    public function afterSave()
    {
        $this->value = CJSON::decode($this->value);
        return parent::afterSave();
    }

}