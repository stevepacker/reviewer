<?php

class Repository_Driver_Git extends Repository_Driver_Abstract
{
    /**
     * Initialize Git_Repository object
     *
     * @return null
     */
    public function init()
    {
        $this->_repo = new Git_Repository();
        $this->_repo->gitPath = Setting::get('system.git.path', '/usr/bin/git');
        $this->_repo->setPath($this->getRepoPath(), true);
    }
}