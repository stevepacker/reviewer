<?php

abstract class Repository_Driver_Abstract extends CComponent
{
    /**
     * @var Project The parent project
     */
    private $_project;

    /**
     * @var mixed
     */
    protected $_repo;

    /**
     * Constructor
     *
     * @param Project $project Parent project
     */
    public function __construct(Project $project) {
        $this->_project = $project;
        $this->init();
    }

    /**
     * Placeholder for init work.  Override this method
     * in Driver if desired.
     *
     * @return null
     */
    abstract public function init();

    /**
     * Magic method to transparently call undefined methods to the driver
     *
     * @param string $name Method name
     * @param array $parameters Called arguments to Method
     * @return mixed
     */
    public function __call($name, $parameters) {
        return call_user_func_array(
                array($this->getRepo(), $name),
                $parameters);
    }

    /**
     * Magic method to transparently call undefined parameters to the driver
     *
     * @param string $name Parameter of Repo driver
     * @return mixed
     */
    public function __get($name) {
        return $this->getRepo()->$name;
    }

    /**
     * Returns the parent Project
     *
     * @return Project Parent project
     */
    public function getProject()
    {
        return $this->_project;
    }

    /**
     * Returns a normalized string for the repo path on the filesystem
     *
     * @param string $name Default: $project->name
     * @return string Normalized directory name of project
     */
    public function getPathName($name = null)
    {
        if (null === $name)
            $name = $this->getProject()->name;

        $path = preg_replace(
                    '/([\-]{2,})/',
                    '-',
                    preg_replace(
                        '/([^a-zA-Z0-9_\-\.])/',
                        '-',
                        $name));
        return $path;
    }

    /**
     * Returns the path on the filesystem to the repository
     *
     * @return string Path to locally checked out repository directory
     */
    public function getRepoPath()
    {
        $path  = Yii::app()->basePath . '/runtime/repos/';

        if (!is_dir($path))
            @mkdir($path, null, true);

        return $path . $this->getPathName();
    }

    /**
     * Returns the read-only repo object
     *
     * @return Git_Repository
     */
    public function getRepo()
    {
        return $this->_repo;
    }
}