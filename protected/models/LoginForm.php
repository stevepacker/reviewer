<?php

class LoginForm extends CFormModel
{
    public $email;
    public $password;
    public $rememberMe;

    private $_identity;

    public function rules()
    {
        return array(
            array('email', 'email'),
            array('email, password', 'required'),
            array('password', 'authenticate'),
            array('rememberMe', 'boolean'),
        );
    }

    /**
     * Authenticates the password.
     * This is the 'authenticate' validator as declared in rules().
     */
    public function authenticate($attribute, $params)
    {
        $this->_identity = new UserIdentity($this->email, $this->password);
        if (!$this->_identity->authenticate())
            $this->addError('password', 'Incorrect password.');
    }

    /**
     * Logs in the user using the given password in the model.
     * @return boolean whether login is successful
     */
    public function login()
    {
        if (null === $this->_identity) {
            $this->_identity = new UserIdentity($this->email, $this->password);
            $this->_identity->authenticate();
        }

        if ($this->_identity->errorCode === UserIdentity::ERROR_NONE) {
            $ten_days = 864000;
            Yii::app()->user->login($this->_identity, $this->rememberMe ? $ten_days : null);
            return true;
        } else {
            return false;
        }
    }
}
